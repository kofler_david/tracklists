PRAGMA encoding = 'UTF-8';

CREATE TABLE user (
  id            INTEGER AUTO_INCREMENT,
  display_name  VARCHAR NOT NULL,
  email         VARCHAR NOT NULL,
  password_hash VARCHAR NOT NULL,
  active        INTEGER NOT NULL DEFAULT FALSE,

  PRIMARY KEY (id),
  UNIQUE (display_name),
  UNIQUE (email)
);

-- INSERT INTO user (display_name, email, password_hash, active) VALUES ('System', 'this.login@wont.work.com', '', TRUE);
-- INSERT INTO user (display_name, email, password_hash, active) VALUES ('Root', 'please@dont.login.com', 'willbechanged', FALSE);

CREATE TABLE album (
  id            INTEGER auto_increment,
  title         VARCHAR NOT NULL,
  slug          VARCHAR NOT NULL,
  path_location VARCHAR,

  PRIMARY KEY (id),
  UNIQUE (title),
  UNIQUE (slug)
);

CREATE TABLE track (
  id            INTEGER auto_increment,
  album_id      INTEGER NOT NULL,
  title         VARCHAR NOT NULL,
  time_begin    VARCHAR(8),
  time_duration VARCHAR(8),
  info          VARCHAR,
  label         VARCHAR,
  flag_liked    INT     NOT NULL DEFAULT '0',

  PRIMARY KEY (id),
  FOREIGN KEY (album_id) REFERENCES album (id)
  ON UPDATE CASCADE ON DELETE CASCADE
);

CREATE TABLE playlist (
  id    INTEGER auto_increment,
  title VARCHAR NOT NULL,
  slug  VARCHAR NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (title),
  UNIQUE (slug)
);

CREATE TABLE playlist_track (
  id          INTEGER auto_increment,
  playlist_id INTEGER NOT NULL,
  track_id    INTEGER NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (playlist_id) REFERENCES playlist (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (track_id) REFERENCES track (id)
  ON UPDATE CASCADE ON DELETE CASCADE
);
