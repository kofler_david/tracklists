define(['jquery'], function ($, undefined) {
    return function (R) {
        $(function () {
            $(R.s_LikeButtons).click(function (e) {
                var button = $(this);
                var trackId = $(this).data(R.k_trackId)
                $.get(R.likeUrl, {id: trackId}, function (data) {
                    if (data.status === 'success') {
                        button.addClass(R.c_hide)
                        button.siblings(R.s_UnlikeButtons).removeClass(R.c_hide)
                    }
                })

                e.preventDefault()
            })

            $(R.s_UnlikeButtons).click(function (e) {
                var button = $(this);
                var trackId = $(this).data(R.k_trackId)
                $.get(R.unlikeUrl, {id: trackId}, function (data) {
                    if (data.status === 'success') {
                        button.addClass(R.c_hide)
                        button.siblings(R.s_LikeButtons).removeClass(R.c_hide)
                    }
                })

                e.preventDefault()
            })
        })
    }
})