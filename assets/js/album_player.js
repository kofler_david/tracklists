define(['jquery', 'jplayer'], function ($) {
    function AlbumPlayer(R, player) {
        this.R = R
        this.player = player

        this.currentTime = 0
        this.currentTrack = -1

        var that = this

        // This event handler is added and removed when an album is loaded or stopped respectively.
        this.trackChangeHandler = function (e) {
            var time = e.jPlayer.status.currentTime
            var track = that.searchTrackNum(time)
            that.currentTime = time
            if (track !== that.currentTrack) {
                that.currentTrack = track
                $(that).trigger('trackchange', track)
            }
        }
    }

    AlbumPlayer.prototype.stop = function () {
        this.album = null
        this.player.jPlayer('stop')
        this.player.off($.jPlayer.event.timeupdate, this.trackChangeHandler)
    }

    AlbumPlayer.prototype.setAlbum = function (album) {
        this.album = album
        this.trackCount = this.album.beginTimes.length

        this.player.jPlayer('setMedia', {
            title: this.album.title,
            mp3: this.album.location
        })

        this.player.on($.jPlayer.event.timeupdate, this.trackChangeHandler)
    }

    AlbumPlayer.prototype.playAlbum = function (album) {
        this.setAlbum(album)
        this.playTrack(0)
    }

    AlbumPlayer.prototype.playTrack = function (trackNumber) {
        if (trackNumber != null) {
            var beginTimes = this.album.beginTimes
            var beginTime = beginTimes[0]
            if (trackNumber >= 0 && trackNumber < beginTimes.length) {
                this.player.jPlayer('play', beginTimes[trackNumber])
            } else {
                this.player.jPlayer('play', beginTime)
            }

        } else {
            this.player.jPlayer('play')
        }
    }

    AlbumPlayer.prototype.searchTrackNum = function (time) {
        var beginTimes = this.album.beginTimes
        for (var i = 0, l = beginTimes.length; i < l; ++i) {
            if (beginTimes[i] > time) {
                return i - 1
            }
        }

        return beginTimes.length - 1
    }

    return AlbumPlayer
})