define(['jquery'], function ($, undefined) {
    function Pagination(R, startPage) {
        this.R = R

        // Pages contain the content.
        this.pages = undefined

        // Items are used to select a page.
        this.paginationItems = undefined

        // currentPage consists of the current page, the corresponding
        // pagination item and its index.
        this.currentPage = undefined

        // The supported number of pages. It is the minimum of the number of items and of pages which match the
        // selectors. It is cached here for stylistic and for performance reasons.
        this.pageCount = undefined

        this.loadPagesAndItemsAndFindCurrent()

        this.selectPage(isNaN(startPage) ? 0 : +startPage)

        var that = this

        $(function () {
            $(that.R.s_paginationItems).each(function (i, item) {
                var index = +$(item).data('page')
                $(item).find(that.R.s_paginationItemsClickableChild).click(function (e) {
                    that.selectPage(index)
                    e.preventDefault()
                })
            })

            $(that.R.s_paginationLeftArrow).click(function (e) {
                that.selectPage(that.currentPage.index - 1)
                e.preventDefault()
            })

            $(that.R.s_paginationRightArrow).click(function (e) {
                that.selectPage(that.currentPage.index + 1)
                e.preventDefault()
            })
        })
    }

    Pagination.prototype.loadPagesAndItemsAndFindCurrent = function () {
        var R = this.R
        this.pages = $(R.s_pages)
        this.paginationItems = $(R.s_paginationItems)
        this.pageCount = Math.min(this.paginationItems.size(), this.pages.size())
        this.findCurrent() // Sets this.currentPage
    }

    Pagination.prototype.selectPage = function (index) {
        if (index >= this.pageCount) {
            index = this.pageCount - 1
        }

        // Do not move this upwards. This way the stuff works better if there are no pages.
        if (index < 0) {
            index = 0
        }

        this.paginationItems.removeClass('current')
        this.pages.addClass('hide')
        this.paginationItems.filter('[data-page=' + index + ']').addClass('current')
        this.pages.filter('[data-page=' + index + ']').removeClass('hide')
        this.findCurrent()

        if (index <= 0) {
            $(this.R.s_paginationLeftArror).addClass('unavailable')
        } else {
            $(this.R.s_paginationLeftArror).removeClass('unavailable')
        }

        if (index >= this.pageCount - 1) {
            $(this.R.s_paginationRightArror).addClass('unavailable')
        } else {
            $(this.R.s_paginationRightArror).removeClass('unavailable')
        }
    }

    Pagination.prototype.findCurrent = function () {
        var currentPaginationItem = this.paginationItems.filter('.current')

        if (this.currentPage == null || currentPaginationItem !== this.currentPage.item) {
            var currentIndex = currentPaginationItem.data('page')
            this.currentPage = {
                item: currentPaginationItem,
                index: currentIndex,
                content: this.pages.filter('[data-page=' + currentIndex + ']')
            }
        }

        return this.currentPage
    }

    return Pagination
})