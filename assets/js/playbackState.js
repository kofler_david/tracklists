define(['jquery', 'jquery-cookie'], function ($) {
    /**
     * 'controlTransferred': triggered when another player instance takes control. The event data consists of the ID of
     * the player who took control.
     * 'trackchange': triggered when somebody changes the current track. It may be preceded by 'controlTransferred'
     * The event data (field 'who') includes the ID of the instance who changed the track. This instance is then
     * responsible for playing the track. The field 'track' contains the current track.
     * @param pageId an identifier for the current player instance. It is assumed to be unique.
     * @param cookieConfig an object containing cookie names and cookie settings.
     * @constructor
     */
    function PlaybackState(pageId, cookieConfig) {
        this.cookies = {
            playerMode: 'playerMode',
            activeInstance: 'activePlayer',
            playbackTime: 'playbackTime',
            currentTrackId: 'currentTrack',
            currentAlbumId: 'album',
            currentPlaylistId: 'playlist',

            sessionCookieConfig: {},
            metadataCookieConfig: {
                expires: 1 // day
            }
        }

        this.pageId = pageId
        this.cookies = $.extend(this.cookies, cookieConfig)

        // This cache should only be used to detect changes!
        this.state = {
            isAnotherInstancePlaying: this.isAnotherInstancePlaying(),
            trackId: this.getCurrentTrack()
        }
    }

    /**
     * Should be called periodically to update the current state.
     * When something changes an event will be fired.
     */
    PlaybackState.prototype.checkState = function () {
        var isAnotherInstancePlaying = this.isAnotherInstancePlaying()
        if (!this.state.isAnotherInstancePlaying && isAnotherInstancePlaying) {
            this.state.isAnotherInstancePlaying = isAnotherInstancePlaying
            $(this).trigger('controlTransferred', this.state.pageId)
        }

        var currentTrack = this.getCurrentTrack()
        var changed = true
        if (currentTrack != null && this.state.trackId !== currentTrack.id) {
            this.state.trackId = currentTrack.id

        } else if (this.state.trackId != null) {
            this.state.trackId = null

        } else {
            changed = false
        }

        if (changed) {
            $(this).trigger('trackchange', currentTrack, this.getIdOfActiveInstance())
        }
    }

    PlaybackState.prototype.isAnotherInstancePlaying = function () {
        // Pure, must not be cached
        var idOfActivePage = this.getIdOfActiveInstance()

        if (idOfActivePage == null) {
            return true
        }

        return idOfActivePage === this.pageId
    }

    PlaybackState.prototype.getIdOfActiveInstance = function () {
        // Pure, must not be cached
        return $.cookie(this.cookies.activeInstance, this.cookies.sessionCookieConfig)
    }

    PlaybackState.prototype.makeCurrentInstanceActive = function () {
        // This is allowed. Other instances must check for such things periodically.
        $.cookie(this.cookies.activeInstance, this.pageId, this.cookies.sessionCookieConfig);
    }

    PlaybackState.prototype.getPlayerMode = function () {
        // Pure, must not be cached.
        var mode = $.cookie(this.cookies.playerMode, this.cookies.metadataCookieConfig)
        if ($.inArray(mode, ['album', 'playlist'])) {
            return mode
        }

        return null
    }

    PlaybackState.prototype.getCurrentAlbum = function () {
        if (this.getPlayerMode() !== 'album') {
            return null
        }

        var albumId = +$.cookie(this.cookies.currentAlbumId, this.cookies.metadataCookieConfig)
        if (albumId == null || isNaN(albumId)) {
            return null
        }

        var ret = {
            id: albumId,
            currentTrackId: null,
            playbackTime: 0
        }

        // If there is no track information, use the playback time to infer the track, and start playing from there.
        // If the playback time contradicts the track information, and the track is valid, play the track
        var trackId = +$.cookie(this.cookies.currentTrackId, this.cookies.metadataCookieConfig)
        if (trackId != null && trackId >= 0) {
            ret.currentTrackId = trackId
        }

        var playbackTime = +$.cookie(this.cookies.playbackTime, this.cookies.metadataCookieConfig)
        if (playbackTime != null && playbackTime >= 0) {
            ret.playbackTime = playbackTime
        }

        return ret
    }

    PlaybackState.prototype.getCurrentPlaylist = function () {
        if (this.getPlayerMode() !== 'playlist') {
            return null
        }

        var playlistId = +$.cookie(this.cookies.currentPlaylistId, this.cookies.metadataCookieConfig)
        if (playlistId == null || isNaN(playlistId)) {
            return null
        }

        var ret = {
            id: playlistId,
            currentTrackId: null,
            playbackTime: 0
        }

        // If there is no track information, start the playlist from the beginning.
        // If there is no track information, but a playback time, then ignore it. It is of no use without a track.
        // If the playback time is invalid for the track, start the track from the beginning.
        var trackId = +$.cookie(this.cookies.currentTrackId, this.cookies.metadataCookieConfig)
        if (trackId == null || isNaN(trackId)) {
            return ret
        }

        ret.currentTrackId = trackId

        var playbackTime = +$.cookie(this.cookies.playbackTime, this.cookies.metadataCookieConfig)
        if (playbackTime != null && playbackTime >= 0) {
            ret.playbackTime = playbackTime
        }

        return ret
    }

    PlaybackState.prototype.getCurrentTrack = function () {
        var mode = this.getPlayerMode()

        if (mode == 'album') {
            var album = this.getCurrentAlbum()
            if (album != null && album.trackId != null) {
                return {
                    id: album.trackId,
                    albumId: album.id,
                    playbackTime: album.playbackTime
                }
            }
        } else if (mode == 'playlist') {
            var playlist = this.getCurrentPlaylist()
            if (playlist != null && playlist.trackId != null) {
                return {
                    id: playlist.trackId,
                    playlistId: playlist.id,
                    playbackTime: playlist.playbackTime
                }
            }
        }

        return null
    }

    PlaybackState.prototype.updateMetadata_TrackInAlbum = function (playbackTime, track, album) {
        this.makeCurrentInstanceActive()
        $.cookie(this.cookies.playerMode, 'album', this.cookies.metadataCookieConfig)
        $.cookie(this.cookies.currentTrackId, track.id, this.cookies.metadataCookieConfig)
        $.cookie(this.cookies.playbackTime, track.begin, this.cookies.metadataCookieConfig)
        $.cookie(this.cookies.currentAlbumId, album.id, this.cookies.metadataCookieConfig)
        $.removeCookie(this.cookies.currentPlaylistId, this.cookies.metadataCookieConfig)
    }

    PlaybackState.prototype.updateMetadata_TrackInPlaylist = function (playbackTime, track, playlist) {
        this.makeCurrentInstanceActive()
        $.cookie(this.cookies.playerMode, 'playlist', this.cookies.metadataCookieConfig)
        $.cookie(this.cookies.currentTrackId, track.id, this.cookies.metadataCookieConfig)
        $.cookie(this.cookies.playbackTime, 0, this.cookies.metadataCookieConfig)
        $.cookie(this.cookies.currentPlaylistId, playlist.id, this.cookies.metadataCookieConfig)
        $.removeCookie(this.cookies.currentAlbumId, this.cookies.metadataCookieConfig)
    }

    return PlaybackState
})