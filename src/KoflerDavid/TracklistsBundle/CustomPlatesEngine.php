<?php

namespace KoflerDavid\TracklistsBundle;

use League\Plates\Engine;

class CustomPlatesEngine extends Engine {

    private $defaultData = [ ];

    /**
     * @param array $defaultData
     */
    public function setDefaultData($defaultData) {
        $this->defaultData = $defaultData;
    }

    /**
     * @return \League\Plates\Template
     */
    public function makeTemplate() {
        $template = parent::make();
        $template->data($this->defaultData);

        return $template;
    }

}