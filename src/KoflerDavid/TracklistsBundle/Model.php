<?php

namespace KoflerDavid\TracklistsBundle;

/**
 * Description of Model
 *
 * @author David Kofler <kofler.david@gmail.com>
 */
abstract class Model implements \JsonSerializable {

    private $attributes = [ ];

    public function __get($name) {
        return $this->getAttribute($name, null);
    }

    public function getAttribute($name, $default = null) {
        if ($this->__isset($name)) {
            return $this->attributes[ $name ];
        } else {
            return $default;
        }
    }

    public function __isset($name) {
        return array_key_exists($name, $this->attributes);
    }

    public function __unset($name) {
        unset($this->attributes[ $name ]);
    }

    public function __set($name, $value) {
        $this->attributes[ $name ] = $value;
    }

    public function mapAndSetAttributes(array $source, array $mappings) {
        foreach ($mappings as $fieldName => $attributeName) {
            if (!is_string($attributeName)) {
                throw new \LogicException('Attribute names must be strings.');
            }

            if (!is_string($fieldName)) {
                $fieldName = $attributeName;
            }

            if (array_key_exists($fieldName, $source)) {
                $this->__set($attributeName, $source[ $fieldName ]);
            }
        }
    }

    public function setAttributes($attributes) {
        foreach ($attributes as $key => $value) {
            $this->__set($key, $value);
        }
    }

}
