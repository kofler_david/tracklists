<?php

namespace KoflerDavid\TracklistsBundle;

use Igorw\Silex\ConfigServiceProvider;
use KoflerDavid\TracklistsBundle\Controllers\AlbumController;
use KoflerDavid\TracklistsBundle\Controllers\PlaylistController;
use KoflerDavid\TracklistsBundle\Controllers\TrackController;
use KoflerDavid\TracklistsBundle\Repositories\AlbumRepository;
use KoflerDavid\TracklistsBundle\Repositories\PlaylistRepository;
use KoflerDavid\TracklistsBundle\Repositories\TrackRepository;
use Silex\Application\UrlGeneratorTrait;
use Silex\Provider;
use Symfony\Component\HttpFoundation\Request;
use Tracklists\Controllers;
use Tracklists\Repositories;

class Application extends \Silex\Application {

    use UrlGeneratorTrait;

    /**
     * This path is used by the Plates Assets extension to cache-bust the asset files.
     */
    const ASSETS_PATH = '/assets';
    const ROUTER_CONFIG_FILE = '/app/config/routes.yml';

    function __construct($values) {
        parent::__construct($values);
        $this->register(new Provider\ServiceControllerServiceProvider());
        $this->register(new Provider\UrlGeneratorServiceProvider());
        $this->register(new ConfigServiceProvider($this['config-file']));
        $this->register(new ConfigServiceProvider($this['app_root'] . self::ROUTER_CONFIG_FILE));
        $this->register(new PlatesServiceProvider());

        $this->fixAssetUrl();
        $this->setupTemplateEngine();
        $this->setupControllers();
        $this->setupDatabaseAndRepositories();
        $this->setupRoutes();

        $this['pageId'] = uniqid();

        // this must be done in the `before` handler because it needs access to the browser cookies
        $this->before(function (Request $request) {
            $this->loadPlayerData($request);
        });
    }

    /**
     * This method has the task to prepend the root URL of the current application to the asset URL if it is an
     * absolute URL. This simplifies administering the application.
     *
     */
    private function fixAssetUrl() {
        $assetUrl = $this['assets-url'];
        if (strlen($assetUrl) > 0 and $assetUrl[0] === '/') {
            $this['assets-url'] = rtrim($this['root-url'], '/') . $assetUrl;
        }
    }

    private function setupTemplateEngine() {
        // This key identifies the template engine being used.
        $this['templating'] = 'plates';

        $this['plates.path'] = __DIR__ . '/Resources/views';
        $this['plates.file_extension'] = 'php';
        $this['plates.default_data'] = [ 'title' => null ];
        $this['plates.extensions'] = [
            new Asset($this['app_root'] . '/' . self::ASSETS_PATH, $this['assets-url']),
            new MultipleSections('header'),
            new MultipleSections('titleBar'),
            new MultipleSections('leftSideBar'),
            new MultipleSections('footer')
        ];
    }

    private function setupControllers() {
        $this['cs.album'] = function () {
            return new AlbumController($this);
        };

        $this['cs.playlist'] = function () {
            return new PlaylistController($this);
        };

        $this['cs.track'] = function () {
            return new TrackController($this);
        };
    }

    private function setupDatabaseAndRepositories() {
        $this['capsule'] = $this->share(function () {
            $capsule = new \Illuminate\Database\Capsule\Manager();
            $capsule->addConnection(include $this['app_root'] . "/app/config/dbconfig.php");
            $capsule->setAsGlobal();

            return $capsule;
        });

        $this['repository.album'] = $this->share(function ($app) {
            return new AlbumRepository($app['capsule']);
        });

        $this['repository.playlist'] = $this->share(function ($app) {
            return new PlaylistRepository($app['capsule']);
        });

        $this['repository.track'] = $this->share(function ($app) {
            return new TrackRepository($app['capsule']);
        });
    }

    private function setupRoutes() {
        $routes = $this['defined_routes'];

        $this->get('/', $routes['home']['controller'])->bind('home');

        foreach ($routes['prefixes'] as $prefix => $routes) {
            $bundle = $this['controllers_factory'];

            foreach ($routes as $route) {
                $result = $bundle->{$route['method']}($route['pattern'], $route['controller']);
                if (array_key_exists('defaults', $route)) {
                    foreach ($route['defaults'] as $param => $value) {
                        $result = $result->value($param, $value);
                    }
                }

                if (array_key_exists('name', $route)) {
                    $result->bind($route['name']);
                }
            }

            $this->mount($prefix, $bundle);
        }
    }

    private function loadPlayerData(Request $request) {
        $this['playbackState.playerType'] = null;

        if ($request->cookies->get('playerType') === 'album') {
            $albumId = $request->cookies->getInt('album');

            if ($albumId !== null) {
                $album = $this['repository.album']->byId($albumId);

                if ($album !== false) {
                    $this['playbackState.playerType'] = 'album';
                    $this['playbackState.album'] = $album;
                }
            }

        } else if ($request->cookies->get('playerType') === 'playlist') {
            $playlistId = $request->cookies->getInt('album');

            if ($playlistId !== null) {
                $playlist = $this['repository.playlist']->byId($playlistId);

                if ($playlist !== false) {
                    $this['playbackState.playerType'] = 'playlist';
                    $this['playbackState.playlist'] = $playlist;
                }
            }
        }

//        fb($this['playbackState.playerType']);
    }

} 