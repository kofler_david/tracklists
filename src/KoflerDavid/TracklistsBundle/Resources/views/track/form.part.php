<?php foreach ($tracks as $i => $track): ?>

    <?php if (is_array($track)): ?>
        <tr>
            <td><input type="text" name="track-<?=$i?>-name" value="<?=$this->escape($track['name'])?>" size="90"></td>
            <td><input type="text" name="track-<?=$i?>-begin" value="<?=$this->escape($track['begin'])?>" size="6"></td>
            <td><input type="text" name="track-<?=$i?>-duration" value="<?=$this->escape($track['duration'])?>"
                       size="6"></td>
            <td><input type="text" name="track-<?=$i?>-label" value="<?=$this->escape($track['label'])?>"></td>
            <td><input type="text" name="track-<?=$i?>-info" value="<?=$this->escape($track['info'])?>"></td>
        </tr>

    <?php else: ?>
        <tr>
            <td><input type="text" name="track-<?=$i?>-name" value="<?=$this->escape($track->getName())?>" size="90">
            </td>
            <td><input type="text" name="track-<?=$i?>-begin"
                       value="<?=$this->escape($track->getBegin()->format('H:i:s'))?>" size="6">
            </td>
            <td><input type="text" name="track-<?=$i?>-duration"
                       value="<?=$this->escape($track->getDuration()->format('%H:%I:%S'))?>" size="6"></td>
            <td><input type="text" name="track-<?=$i?>-label" value="<?=$this->escape($track->getLabel())?>"></td>
            <td><input type="text" name="track-<?=$i?>-info" value="<?=$this->escape($track->getInfo())?>"></td>
        </tr>

    <?php endif; ?>

<?php endforeach; ?>
