<?php $this->layout('template', [ 'title' => $album->getName() ]) ?>

<?php if ($showPlayer): ?>
    <?php //$this->insert('player.part', [ 'album' => $album, 'outer' => $this ]) ?>
<?php endif ?>

<?php $this->start('titleSection') ?>
<h1>
    <?=$this->escape($album->getName())?>
    <a href="<?=$app->path('album-show', [ 'slug' => $album->slug ])?>?format=csv"
       data-tooltip class="has-tip" title="Export to CSV" aria-haspopup="true">
        <img alt="Export as CSV" src="<?=$this->asset('/img/text-csv.png')?>">
    </a>
</h1>
<?php $this->stop() ?>

<table id="playlist">
    <thead>
    <tr>
        <th>&nbsp;</th>
        <th>Name</th>
        <th>Begin</th>
        <th>Duration</th>
        <th>Info</th>
        <th>Label</th>
    </tr>
    </thead>

    <tbody>
    <?php foreach ($album->getTracks() as $i => $track): ?>
        <tr class="track" data-position="<?=$i + 1?>">
            <td>
                <a class="likeButton<?=$track->isLiked() ? ' hide' : ''?>" data-trackId="<?=$track->id?>" href="#">
                    <img alt="Like track" src="<?=$this->asset('/img/edit-add-4.png')?>">
                </a>
                <a class="unlikeButton<?=$track->isLiked() ? '' : ' hide'?>" data-trackId="<?=$track->id?>" href="#">
                    <img alt="Like track" src="<?=$this->asset('/img/bookmark-3.png')?>">
                </a>
                <img alt="Current Track" src="<?=$this->asset('/img/bullet-blue.png')?>" class="currentTrackBullet">
            </td>
            <td><a href="#" class="trackTitle"><?=$this->escape($track->getName())?></a></td>
            <td><?=$track->getBegin()->format('H:i:s')?></td>
            <td><?=$track->getDuration()->format('%H:%I:%S')?></td>
            <td><?=$this->escape($track->getInfo())?></td>
            <td><?=$this->escape($track->getLabel())?></td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>

<?php $this->footer()->start() ?>
<script type="text/javascript">
    require(['jquery', 'app/trackLikeButton'], function ($, trackLikeButtons) {
        var R = {
            s_LikeButtons: '.likeButton',
            s_UnlikeButtons: '.unlikeButton',
            k_trackId: 'trackid',
            likeUrl: '<?=$app->path('track-like')?>',
            unlikeUrl: '<?=$app->path('track-unlike')?>',
            c_hide: 'hide'
        }

        trackLikeButtons(R)
    })
</script>
<script type="text/javascript">
    require(['jquery', 'app/player_integration'], function ($, bootstrap) {
        $(function () {
            var urls = {
                assetsUrl: <?=json_encode($app['assets-url'])?>,
                rootUrl: <?=json_encode($app['root-url'])?>,
                ajaxGetAlbum: <?=json_encode($app->path('album-get-ajax'))?>
            }

            var player = bootstrap($('html').data('pageId'), urls)

            $('.track').each(function () {
                var trackNumber = $(this).data('position')
                $(this).find('a.trackTitle').click(function (e) {

                    player.playAlbumTrack(<?=$album->id?>, trackNumber)
                    e.preventDefault()
                })
            })
        })
    })
</script>
<?php $this->footer()->stop() ?>
