<?php $this->layout('template', [ 'title' => 'All Albums' ]) ?>
<?php if (count($albums) > 0): ?>
    <dl id="album-accordion" class="accordion" data-accordion>
        <?php foreach ($albums as $i => $album): ?>
            <dd class="accordion-navigation">
                <a class="opener" href="#album-detail-<?=$i + 1?>">
                    <span><?=$this->escape($album->getName())?></span>
                    <img class="accordion-opener right" src="<?=$this->asset('/img/arrow-down-custom.png')?>"
                         alt="Show album">
                </a>

                <div class="content" id="album-detail-<?=$i + 1?>" data-album-id="<?=$album->id?>">
                    <ul class="button-group round tiny">
                        <li>
                            <a class="button tiny"
                               href="<?=$app->path('album-show', [ 'slug' => $album->slug ])?>">
                                <img src="<?=$this->asset('/img/cd-go.png')?>" alt="Export to CSV">
                            </a>
                        </li>
                        <li>
                            <a class="button tiny" href="">
                                <img src="<?=$this->asset('/img/media-playback-start-8.png')?>" alt="Play Album">
                            </a>
                        </li>
                        <li>
                            <a class="button tiny"
                               href="<?=$app->path('album-show', [ 'slug' => $album->slug ])?>?format=csv">
                                <img src="<?=$this->asset('/img/document-export-4.png')?>" alt="Export to CSV">
                            </a>
                        </li>
                    </ul>

                    <div class="album-detail-container">
                        <div class="in-progress-message alert-box info">The album is being loaded...</div>
                        <div class="error-message alert-box alert">The album could not be loaded.</div>
                    </div>
                </div>
            </dd>
        <?php endforeach ?>
    </dl>

    <?php $this->footer()->start() ?>
    <script type="text/javascript">
        require(['jquery', 'app/trackLikeButton', 'foundation'], function ($, trackLikeButtons) {
            var R = {
                s_albumAccordion: '#album-accordion',
                s_albumAccordionContent: '#album-accordion .content',
                s_albumDetailContainer: '.album-detail-container',
                url_getRenderedAlbum: <?=json_encode($app->path('album-render-ajax'))?>
            }

            var R_trackLikeButtons = {
                // These two are intentionally commented out because they must be set before trackLikeButtons is called.
                //s_LikeButtons: '.likeButton',
                //s_UnlikeButtons: '.unlikeButton',
                k_trackId: 'trackId',
                likeUrl: '<?=$app->path('track-like')?>',
                unlikeUrl: '<?=$app->path('track-unlike')?>',
                c_hide: 'hide'
            }

            $(function () {
                $(R.s_albumAccordionContent).find('.alert-box').hide()

                $(R.s_albumAccordion).on('toggled', function (event, accordion) {
                    if (!accordion.data('tracksLoaded')) {
                        var albumId = $(accordion).data('albumId')
                        // It is necessary to encode the parameters explicitly.
                        // If parameters are passed as an object $.load() uses POST, which would make no sense.
                        var parameters = $.param({'id': albumId})
                        accordion.find(R.s_albumDetailContainer)
                            .load(R.url_getRenderedAlbum, parameters, function (responseText, testStatus, jqXHR) {
                                if (testStatus == 'success') {
                                    accordion.data('tracksLoaded', true)

                                    // Enable the "Like" and "Unlike" buttons on the tracklist.
                                    // It must be ensured that the plugin only operates on the newly loaded ones!
                                    R_trackLikeButtons.s_LikeButtons = '#' + accordion.attr('id') + ' .likeButton'
                                    R_trackLikeButtons.s_UnlikeButtons = '#' + accordion.attr('id') + ' .unlikeButton'
                                    trackLikeButtons(R_trackLikeButtons)
                                    // Nothing else to do because $().load() already replaced the alerts.
                                } else {
                                    $(this).find('.in-progress-message').hide()
                                    $(this).find('.error-message').show()
                                }
                            })

                        $(this).find('.in-progress-message').show()
                    }
                })
            })
        })
    </script>
    <?php $this->footer()->stop() ?>
<?php else: ?>
    <span data-alert class="alert-box info">There are no albums.</span>
<?php endif ?>
