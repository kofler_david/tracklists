<?php $this->layout('template', [ 'title' => 'Parse Album' ]) ?>

<form method="post" action="<?=$app->path('album-parse-post')?>">
    <div class="row">
        <div class="medium-9 large-10 columns">
            <label for="album-name">Album name</label>
            <input type="text" id="album-name" name="album-name" value="<?=$this->escape($albumName)?>" required>
        </div>

        <div class="medium-3 large-2 columns">
            <label for="totalDuration">Total duration</label>
            <input type="time" name="totalDuration" value="<?=$this->escape($totalDuration)?>" required>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <label for="album-input">Tracklist to parse</label>
            <textarea id="album-input" name="input" rows="10" cols="50"><?=$this->escape($input)?></textarea>
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            &nbsp;
        </div>
    </div>

    <div class="row">
        <div class="small-12 columns">
            <button class="button" type="submit" name="action" value="parse">
                Parse Album
            </button>
        </div>
    </div>

    <?php $this->insert('album/location.part', [ 'outer' => $this, 'incomingFiles' => $incomingFiles ]) ?>

    <?php if (count($tracks) > 0): ?>
        <fieldset>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Begin</th>
                    <th>Duration</th>
                    <th>Label</th>
                    <th>Comment</th>
                </tr>
                <?php $this->insert('track/form.part', [ 'tracks' => $tracks ]) ?>
            </table>

            <button type="submit" name="action" value="save">Save album</button>
        </fieldset>
    <?php endif ?>
</form>