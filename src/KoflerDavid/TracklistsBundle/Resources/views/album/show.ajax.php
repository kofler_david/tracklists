<table class="table">
    <thead>
    <tr class="hide">
        <th>Currently played</th>
        <th>Title</th>
        <th>Begin</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($album->getTracks() as $i => $track): ?>
        <tr class="track" data-position="<?=$i + 1?>">
            <td>
                <a class="likeButton<?=$track->isLiked() ? ' hide' : ''?>" data-track-id="<?=$track->id?>" href="#">
                    <img alt="Like track" src="<?=$this->asset('/img/edit-add-4.png')?>">
                </a>
                <a class="unlikeButton<?=$track->isLiked() ? '' : ' hide'?>" data-track-id="<?=$track->id?>" href="#">
                    <img alt="Like track" src="<?=$this->asset('/img/bookmark-3.png')?>">
                </a>
                <img alt="Current Track" src="<?=$this->asset('/img/bullet-blue.png')?>" class="currentTrackBullet">
            </td>
            <td><a href="#" class="trackTitle"><?=$this->escape($track->getName())?></a></td>
            <td><?=$track->getBegin()->format('H:i:s')?></td>
        </tr>
    <?php endforeach ?>
    </tbody>
</table>