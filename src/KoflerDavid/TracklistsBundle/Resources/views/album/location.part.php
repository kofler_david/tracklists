<div class="row" id="serverPathTab">
    <?php if (count($incomingFiles) > 1): ?>
        <ul class="pagination">
            <li class="arrow unavailable">
                <a href="#">&laquo;</a>
            </li>

            <?php foreach (array_keys($incomingFiles) as $index): ?>
                <li data-page="<?=$index?>"<?=($index === 0 ? ' class="current"' : '')?>>
                    <a href="#"><?=$index + 1?></a>
                </li>
            <?php endforeach ?>

            <li class="arrow">
                <a href="#">&raquo;</a>
            </li>
        </ul>
    <?php endif ?>

    <?php if (count($incomingFiles) > 0): ?>
        <table id="incomingServerFiles">
            <thead>
            <tr>
                <th>&nbsp;</th>
                <th>Name</th>
                <th>Size</th>
            </tr>
            </thead>

            <?php $idCounter = 0 ?>
            <?php foreach ($incomingFiles as $index => $page): ?>
                <tbody data-page="<?=$index?>">
                <?php foreach ($page as $i => $file): ?>
                    <tr>
                        <td>
                            <input type="radio" name="serverFile" id="server_file_<?=$idCounter?>"
                                   value="<?=$this->e($file['name'])?>">
                        </td>
                        <td>
                            <label for="server_file_<?=$idCounter?>"><?=$this->e($file['name'])?></label>
                        </td>
                        <td>
                            <?=$file['size']?>
                        </td>
                    </tr>
                    <?php ++$idCounter ?>
                <?php endforeach ?>
                </tbody>
            <?php endforeach ?>

        </table>
    <?php else: ?>
        <div class="alert-box info">There are no files in your <strong>incoming</strong> directory.</div>
    <?php endif ?>
</div>

<?php $outer->footer()->start() ?>
<script type="text/javascript">
    require(['jquery', 'app/pagination'], function ($, Pagination) {
        var R = {
            s_pages: '#incomingServerFiles tbody',
            s_paginationItems: '#serverPathTab .pagination [data-page]',
            s_paginationItemsClickableChild: 'a',
            s_paginationLeftArrow: '#serverPathTab .pagination :first',
            s_paginationRightArrow: '#serverPathTab .pagination :last'
        }

        var pagination = new Pagination(R)
    })
</script>
<?php $outer->footer()->stop() ?>