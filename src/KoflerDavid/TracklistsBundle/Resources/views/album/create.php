<?php $this->layout('template', [ 'title' => 'New Album' ]) ?>

<form method="post" action="<?=$app->path('album-create-post')?>" id="newAlbumForm">
    <div class="row">
        <label for="album-name">Album name</label>
        <input type="text" id="album-name" name="album-name" value="<?=$this->escape($albumName)?>">
    </div>

    <input type="hidden" id="trackCount" name="track-count" value="<?=count($tracks)?>"/>

    <div class="row">
        &nbsp;
    </div>

    <div class="row">
        <table id="tracks">
            <thead>
            <tr>
                <th>Name</th>
                <th>Begin</th>
                <th>Duration</th>
                <th>Label</th>
                <th>Comment</th>
            </tr>
            </thead>
            <tbody>
            <?php if (count($tracks) > 0): ?>
                <?php $this->insert('track/form.part', [ 'tracks' => $tracks ]); ?>
            <?php else: ?>
                <tr id="firstTrackRow">
                    <td><input type="text" name="track-1-name" value="" size="90"></td>
                    <td><input type="text" name="track-1-begin" value="" size="6"></td>
                    <td><input type="text" name="track-1-duration" value="" size="6"></td>
                    <td><input type="text" name="track-1-label" value=""></td>
                    <td><input type="text" name="track-1-info" value=""></td>
                </tr>
            <?php endif; ?>
            </tbody>
        </table>
    </div>

    <div class="row">
        <ul class="button-group">
            <li>
                <a href="#" class="button" id="addTrackButton">Add Track</a>
            </li>
            <li>
                <button type="submit">Parse Album</button>
            </li>
        </ul>
    </div>
</form>

<?php $this->footer()->start() ?>
<script type="text/javascript">
    require(['jquery'], function ($) {
        $(function () {
            $('#addTrackButton').click(function () {
                var newRow = $('#firstTrackRow').clone()
                // Remove the id
                newRow.removeAttr('id')

                var trackNum = $('#tracks').find('tbody').children().size() + 1
                var prefix = 'track-' + trackNum + '-'
                $(newRow).find('input[name=track-1-name]')[0].setAttribute('name', prefix + 'name')
                $(newRow).find('input[name=track-1-begin]')[0].setAttribute('name', prefix + 'begin')
                $(newRow).find('input[name=track-1-label]')[0].setAttribute('name', prefix + 'label')
                $(newRow).find('input[name=track-1-info]')[0].setAttribute('name', prefix + 'info')
                $('#tracks').append(newRow)
                // Set the correct track count for processing the form.
                $('#trackCount').val(trackNum)
            })

            $('#newAlbumForm').submit(function () {
                // The field `trackCount` has only a correct value if the user entered
                // at least two track. If the field contains the value '0' then we
                // know that there is only one form row, but not if it contains any
                // valid data.
                if ($('#trackCount').val() === '0') {
                    var incrementTrackCount = false

                    if ($.trim($(this).find('input[name=track-1-name]').val()) !== '') {
                        incrementTrackCount = true
                    }

                    if ($.trim($(this).find('input[name=track-1-begin]').val()) !== '') {
                        incrementTrackCount = true
                    }

                    if ($.trim($(this).find('input[name=track-1-label]').val()) !== '') {
                        incrementTrackCount = true
                    }

                    if ($.trim($(this).find('input[name=track-1-info]').val()) !== '') {
                        incrementTrackCount = true
                    }

                    // If the form row contained data then the field `trackCount`
                    // has to be adjusted.
                    if (incrementTrackCount) {
                        $('#trackCount').val()
                    }
                }

                return true;
            })
        })
    })
</script>
<?php $this->footer()->stop() ?>
