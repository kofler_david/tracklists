<!DOCTYPE html>
<html data-page-id="<?=$app['pageId']?>">

<head profile="http://www.w3.org/2005/10/profile">
    <meta charset="UTF-8">
    <title>Albums<?=$title !== null ? ' - ' . $this->escape($title) : ''?></title>
    <link rel="icon" type="image/png" href="<?=$this->asset('/img/emblem-music.png')?>">
    <link rel="stylesheet" type="text/css" href="<?=$this->asset('/components/require.css')?>"/>
    <link rel="stylesheet" type="text/css" href="<?=$this->asset('/css/layout.css')?>">
    <?php $this->header()->insert() ?>
</head>

<body>

<?php $this->insert('player.part', [ 'outer' => $this ]) ?>

<div class="fixed">
    <nav id="header" class="top-bar" data-topbar role="navigation">
        <ul class="title-area">
            <li class="name">
                <h1>
                    <a href="<?=$app->path('home')?>">Albums</a>
                </h1>
            </li>
        </ul>

        <section class="top-bar-section">
            <ul>
                <li class="has-dropdown">
                    <a href="#">Add Album</a>
                    <ul class="dropdown">
                        <li>
                            <a href="<?=$app->path('album-create')?>">
                                Create new Album
                            </a>
                        </li>
                        <li>
                            <a href="<?=$app->path('album-parse')?>">
                                Parse Album from Tracklist
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="has-dropdown">
                    <a href="<?=$app->path('playlist-index')?>">
                        Playlists
                    </a>
                    <ul class="dropdown">
                        <li>
                            <a data-reveal-id="newPlaylist" data-reveal-ajax="true"
                               href="<?=$app->path('playlist-new')?>?idPrefix=new-playlist-&destination=<?=rawurlencode($app['request']->server->get('REQUEST_URI'))?>">
                                Create new Playlist
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </section>

        <?php $this->titleBar()->insert() ?>
    </nav>
</div>

<div class="row">
    <nav id="side-nav" class="medium-3 large-2 columns">
        <ul class="side-nav">
            <?php $this->leftSideBar()->insert() ?>
        </ul>
    </nav>

    <div id="content" class="medium-9 large-10 columns">
        <?php if ($this->section('title')): ?>
            <?=$this->section('title')?>
        <?php else: ?>
            <h1><?=$this->escape($title)?></h1>
        <?php endif ?>

        <?php if (count($errors) > 0): ?>
            <hr>

            <?php foreach ($errors as $error): ?>
                <div class="alert-box alert" data-alert><?=$this->escape($error)?></div>
            <?php endforeach ?>

            <hr>
        <?php endif ?>

        <?=$this->section('content')?>
    </div>
</div>

<div id="newPlaylist" class="reveal-modal tiny" data-reveal></div>

<script type="text/javascript" src="<?=$this->asset('/components/require.js')?>"></script>
<script type="text/javascript">
    requirejs.config({
        baseUrl: '<?=$this->directory('/components')?>',
        packages: [
            {
                name: 'jplayer',
                main: 'jquery.jplayer/jquery.jplayer'
            }
        ],
        paths: {
            'app': '<?=$this->directory('/js')?>'
        },
        shim: {
            'jplayer': ['jquery'],
            'app/tracklist_player': ['jquery', 'jplayer']
        }
    })

    require(['jquery', 'foundation'], function ($) {
        $(document).foundation()
    })
</script>
<?php $this->footer()->insert() ?>
</body>

</html>