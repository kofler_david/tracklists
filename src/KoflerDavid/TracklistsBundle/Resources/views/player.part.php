<?php $outer->titleBar()->start() ?>
<section class="top-bar-section" id="jplayer_player">
    <div class="right">
        <ul>
            <li>
                <a href="#" class="previous">
                    <img alt="Previous track" src="<?=$this->asset('/img/media-seek-backward-2.png')?>">
                </a>
            </li>
            <li>
                <a href="#" class="play" data-action="play">
                    <img alt="Previous track" src="<?=$this->asset('/img/media-playback-start-2.png')?>">
                </a>
            </li>
            <li>
                <a href="#" class="pause">
                    <img alt="Previous track" src="<?=$this->asset('/img/media-playback-pause-2.png')?>'">
                </a>
            </li>
            <li>
                <a href="#">
                    <span id="currentTime"></span> / <span id="duration"></span>
                </a>
            </li>
            <li>
                <a href="#" class="stop">
                    <img alt="Previous track" src="<?=$this->asset('/img/media-playback-stop-2.png')?>">
                </a>
            </li>
            <li>
                <a href="#" class="next">
                    <img alt="Previous track" src="<?=$this->asset('/img/media-seek-forward-2.png')?>">
                </a>
            </li>
        </ul>
    </div>
</section>
<?php $this->titleBar()->stop() ?>

<div id="jplayer_player_area"></div>

<div class="jp-no-solution">
    <span>Update Required</span>
    To play the media you will need to either update your browser to a recent version or update your <a
        href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
</div>

<?php $outer->footer()->start() ?>
<?php fb($app['root-url'])?>
<script type="text/javascript">
    require(['jquery', 'app/player_integration'], function ($, bootstrap) {
        var urls = {
            assetsUrl: <?=json_encode($app['assets-url'])?>,
            rootUrl: <?=json_encode($app['root-url'])?>,
            ajaxGetAlbum: <?=json_encode($app->path('album-get-ajax'))?>
        }

        bootstrap($('html').data('pageId'), urls)
    })
</script>
<?php $outer->footer()->stop() ?>
