<?php $this->layout('template', [ 'title' => $playlist->getTitle() ]) ?>

<?php if (count($playlist->getTracks()) > 0): ?>
    <table>
        <thead>
        <tr>
            <th>&nbsp;</th>
            <th>Name</th>
            <th>Duration</th>
            <th>Info</th>
            <th>Label</th>
        </tr>
        </thead>

        <tbody>
        <?php foreach ($playlist->getTracks() as $i => $track): ?>
            <tr class="track" data-position="<?=$i + 1?>">
                <td>
                    <a class="likeButton<?=$track->isLiked() ? ' hide' : ''?>" data-trackId="<?=$track->id?>" href="#">
                        <img alt="Like track" src="<?=$this->asset('/img/edit-add-4.png')?>">
                    </a>
                    <a class="unlikeButton<?=$track->isLiked() ? '' : ' hide'?>" data-trackId="<?=$track->id?>"
                       href="#">
                        <img alt="Like track" src="<?=$this->asset('/img/bookmark-3.png')?>">
                    </a>
                </td>
                <td><a href="#" class="trackTitle"><?=$this->escape($track->getTitle())?></a></td>
                <td><a href="#"><?=$track->getDuration()->format('%H:%I:%S')?></td>
                <td><a href="#"><?=$this->escape($track->getInfo())?></a></td>
                <td><a href="#"><?=$this->escape($track->getLabel())?></a></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
<?php else: ?>
    <span class="alert info">This playlist contains no tracks yet.</span>
<?php endif ?>

<?php $this->footer()->start() ?>
    <script type="text/javascript">
        require(['jquery', 'app/trackLikeButton'], function ($, trackLikeButtons) {
            var R = {
                s_LikeButtons: '.likeButton',
                s_UnlikeButtons: '.unlikeButton',
                k_trackId: 'trackid',
                likeUrl: '<?=$app->path('track-like')?>',
                unlikeUrl: '<?=$app->path('track-unlike')?>',
                c_hide: 'hide'
            }

            trackLikeButtons(R)
        })
    </script>
<?php $this->footer()->stop() ?>