<h3>Create playlist</h3>

<?php $this->insert('playlist/new.form', [ 'outer' => $this, 'idPrefix' => $idPrefix, 'destination' => $destination ]) ?>

<a class="close-reveal-modal" id="<?=$idPrefix?>close-modal">&#215;</a>

<div id="<?=$idPrefix?>creation-in-progress-modal" class="reveal-modal tiny" data-reveal>
    <div class="alert-box info">The Playlist is being created...</div>
</div>

<div id="<?=$idPrefix?>creation-successful-modal" class="reveal-modal tiny" data-reveal>
    <div class="alert-box success">The playlist was successfully created!</div>
    <a class="close-reveal-modal">&#215;</a>
</div>

<div id="<?=$idPrefix?>creation-failed-modal" class="reveal-modal tiny" data-reveal>
    <div class="alert-box alert">The playlist could not be created.</div>
    <a class="close-reveal-modal">&#215;</a>
</div>

<div id="<?=$idPrefix?>duplicate-name-modal" class="reveal-modal alert tiny" data-reveal>
    <div class="alert-box alert">A playlist with the same name already exists!</div>
</div>

<script type="text/javascript">
    require(['jquery', 'foundation'], function ($) {
        var R = {
            s_playlistForm: <?=json_encode('#'.$idPrefix.'playlistForm')?>,
            s_closeButton:  <?=json_encode('#'.$idPrefix.'close-modal')?>,
            s_emptyTitleModal: <?=json_encode('#'.$idPrefix.'empty-title-modal')?>,
            s_creationSuccessfulModal: <?=json_encode('#'.$idPrefix.'creation-successful-modal')?>,
            s_creationInProgressModal: <?=json_encode('#'.$idPrefix.'creation-in-progress-modal')?>,
            s_creationFailedModal: <?=json_encode('#'.$idPrefix.'creation-failed-modal')?>,
            url_createPlaylist: <?=json_encode($app->path('playlist-create'))?>
        }

        $(function () {
            // When the title is valid and the user submits the form, prevent submission and do it over AJAX instead.
            // when finished display the success modal.
            $(R.s_playlistForm).submit(function (event) {
                $(R.s_creationInProgressModal).foundation('reveal', 'open')

                $.post(R.url_createPlaylist, $(this).serialize())
                    .success(function () {
                        $(R.s_creationInProgressModal).foundation('reveal', 'close')
                        $(R.s_creationSuccessfulModal).foundation('reveal', 'open')
                    })
                    .fail(function () {
                        $(R.s_creationInProgressModal).foundation('reveal', 'close')
                        $(R.s_creationFailedModal).foundation('reveal', 'open')
                    })

                // Stop the from submitting the data using POST
                event.preventDefault()
            })

            // When the user sees the success message and closes the modal then the first modals shall also be closed.
            $(R.s_creationSuccessfulModal).find('.close-reveal-modal').click(function () {
                $(R.s_closeButton).click()
            })
        })
    })
</script>