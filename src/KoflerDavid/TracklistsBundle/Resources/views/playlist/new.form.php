<form method="post" action="<?=$app->path('playlist-create')?>" id="<?=$idPrefix?>playlistForm">
    <label for="<?=$idPrefix?>playlistTitle">Playlist title</label>
    <input type="text" name="title" id="<?=$idPrefix?>playlistTitle" required>
    <small class="error" id="<?=$idPrefix?>error-message">Please provide a Playlist title</small>
    <button type="submit" id="<?=$idPrefix?>createButton"> Create Playlist</button>
    <input type="hidden" name="destination" value="<?=$this->e($destination)?>">
</form>

<?php $outer->footer()->start() ?>
<script type="text/javascript">
    require(['jquery'], function ($) {
        var R = {
            s_playlistTitle: <?=json_encode('#'.$idPrefix.'playlistTitle')?>,
            s_playlistTitleLabel: <?=json_encode('label[for='.$idPrefix.'playlistTitle]')?>,
            s_playlistTitleError: <?=json_encode('#'.$idPrefix.'error-message')?>,
            s_playlistForm: <?=json_encode('#'.$idPrefix.'playlistForm')?>,
            url_createPlaylist: <?=json_encode($app->path('playlist-create'))?>
        }

        $(function () {
            $(R.s_playlistTitleError).hide()
            $(R.s_playlistForm).submit(function (event) {
                // Validate the form data.
                // $.trim() converts `null` to `''`
                var title = $.trim($(R.s_playlistTitle).val())
                if (title.length === 0) { // Should happen only on older browsers
                    $(R.s_playlistTitleLabel).addClass('error')
                    $(R.s_playlistTitle).addClass('error')
                    $(R.s_playlistTitleError).show()
                    event.stopImmediatePropagation()
                    event.preventDefault()
                }
            })
        })
    })
</script>
<?php $outer->footer()->stop() ?>