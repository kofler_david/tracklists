<?php $this->layout('template', [ 'title' => 'All Playlists' ]) ?>

<?php if (count($playlists) > 0): ?>
    <ul>
        <?php foreach ($playlists as $playlist): ?>
            <li>
                <a href="<?=$app->path('playlist-show', [ 'slug' => $playlist->slug ])?>">
                    <?=$this->escape($playlist->getTitle())?>
                </a>
            </li>
        <?php endforeach ?>
    </ul>
<?php else: ?>
    <span data-alert class="alert-box info">There are no playlists.</span>
<?php endif ?>