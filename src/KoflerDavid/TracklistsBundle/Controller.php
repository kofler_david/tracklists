<?php

namespace KoflerDavid\TracklistsBundle;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller
 *
 * @author David Kofler <kofler.david@gmail.com>
 */
class Controller {

    /**
     * @var Application
     */
    protected $app;

    /**
     * These errors are displayed at the top of the page. Only the values,
     * not the keys, will be displayed.
     * @var array
     */
    protected $errors = [ ];

    /**
     * These errors are created when a form is parsed. Here the key <strong>is</strong> important.
     * @var array
     */
    protected $formErrors = [ ];

    /**
     * @param Application $app
     */
    public function __construct(Application $app) {
        $this->app = $app;
        $this->bundle = $app['controllers_factory'];
    }

    /**
     * This function renders a template.
     * The given title is set and the specified errors are appended to the error list.
     * @param string $template
     * @param string $title
     * @param array $data
     * @param int $httpStatus
     * @return Response
     */
    protected function render($template, $title, array $data = [ ], $httpStatus = 200) {
        $data['title'] = $title;
        if (array_key_exists('errors', $data)) {
            $data['errors'] = array_replace($data['errors'], $this->errors);
        } else {
            $data['errors'] = $this->errors;
        }

        $templating = $this->app[ $this->app['templating'] ];
        $templating->addData([ 'errors' => $this->errors ]);
        $content = $templating->render($template, $data);

        return new Response($content, $httpStatus);
    }

    /**
     * AJAX actions should use this method to generate JSON responses.
     *
     * @param mixed $data the AJAX result data.
     * @param string $message If the application is in debug mode then this message will be send as a HTTP header to the
     * browser.
     * @param int $httpStatus The HTTP status code. Default is 200.
     * @return JsonResponse
     */
    protected function ajaxResponse($data, $message = null, $httpStatus = 200) {
        if ($message !== null) {
            return $this->app->json($data, $httpStatus, [ 'AJAX-Debug' => $message ]);
        } else {
            return $this->app->json($data, $httpStatus);
        }
    }

}
