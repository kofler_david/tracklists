<?php

namespace KoflerDavid\TracklistsBundle\Repositories;

use KoflerDavid\TracklistsBundle\DateUtility;
use KoflerDavid\TracklistsBundle\Entity\Track;
use KoflerDavid\TracklistsBundle\Repository;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class TrackRepository extends Repository {

    /**
     * Creates a Track entity out of a database record.
     * @param array $trackRecord
     * @param array $attributes these specify which database field values shall be saved in the entity's attributes.
     * The key/value pairs define a mapping from database fields to attributes. If an item has no key then the field name and attribute name are considered to be the same.
     * @return \KoflerDavid\TracklistsBundle\Entity\Track
     */
    public static function constructTrackFromDatabase(array $trackRecord, array $attributes = [ ]) {
        $begin = \DateTime::createFromFormat('H:i:s', $trackRecord['time_begin']);
        $duration = DateUtility::parseDateInterval($trackRecord['time_duration']);

        $track = new Track($trackRecord['title'], $begin, $duration, $trackRecord['label'], $trackRecord['info'], $trackRecord['flag_liked'] == 1);
        $track->mapAndSetAttributes($trackRecord, $attributes);

        return $track;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function trackLike($id) {
        return $this->dbhandle->table('track')->where('id', $id)->update([ 'flag_liked' => true ]) > 0;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function trackUnlike($id) {
        return $this->dbhandle->table('track')->where('id', $id)->update([ 'flag_liked' => false ]) > 0;
    }

    /**
     * @param \KoflerDavid\TracklistsBundle\Entity\Track $track
     * @param int|null $albumId
     * @return bool
     */
    public function storeTrack(Track $track, $albumId = null) {
        if ($albumId === null) {
            $albumId = $track->album_id;
        }

        $record = [ 'title'         => $track->getName(), 'time_begin' => $track->getBegin()->format('H:i:s'),
                    'time_duration' => $track->getDuration()->format('%H:%I:%S'),
                    'label'         => $track->getLabel(), 'info' => $track->getInfo(),
                    'album_id'      => $albumId ];
        $id = $this->dbhandle->table('track')->insert($record);
        if ($id) {
            $track->id = $id;

            return true;
        } else {
            return false;
        }
    }

}
