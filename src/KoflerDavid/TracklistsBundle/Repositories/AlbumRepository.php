<?php

namespace KoflerDavid\TracklistsBundle\Repositories;

use Illuminate\Database\QueryException;
use KoflerDavid\TracklistsBundle\Entity\Album;
use KoflerDavid\TracklistsBundle\Repository;
use KoflerDavid\TracklistsBundle\SlugUtility;

class AlbumRepository extends Repository {

    /**
     * @param int $id
     * @throws QueryException
     * @return false|\KoflerDavid\TracklistsBundle\Entity\Album
     */
    public function byId($id) {
        $album = $this->dbhandle->table('album')->find($id);

        if ($album) {
            $records = $this->dbhandle->table('track')->where('album_id', '=', $album['id'])->orderBy('time_begin', 'asc')->get();

            return self::constructAlbumFromDatabase($album, $records);
        }

        return false;
    }

    /**
     * Retrieve all albums from the database, without the tracks.
     * @throws QueryException
     * @return \KoflerDavid\TracklistsBundle\Entity\Album[]
     */
    public function getAlbumList() {
        $records = $this->dbhandle->table('album')->get();

        $albums = [ ];
        foreach ($records as $record) {
            $album = new Album($record['title'], $record['slug'], [ ], $record['path_location']);
            $album->id = $record['id'];
            $album->slug = $record['slug'];
            $albums[] = $album;
        }

        return $albums;
    }

    /**
     * Retrieve an album from the database with the associated tracks.
     * @param string $slug the slug of the album
     * @throws QueryException
     * @return false|\KoflerDavid\TracklistsBundle\Entity\Album the album as an associative array with the keys
     * 'id', 'slug', 'name' and 'tracks'. Each track is an associative array
     * with the keys 'name', 'begin', 'info' and 'label'.
     */
    public function getAlbum($slug) {
        $albumRecords = $this->dbhandle->table('album')->where('slug', '=', $slug)->first();

        if ($albumRecords) {
            $trackRecords = $this->dbhandle->table('track')->where('album_id', '=', $albumRecords['id'])->orderBy('time_begin', 'asc')->get();

            return self::constructAlbumFromDatabase($albumRecords, $trackRecords);
        } else {
            return false;
        }
    }

    /**
     * Generates a slug out of a name. If it already exists it appends a number until the slug is unique.
     * @param string $name the name of the album
     * @throws QueryException when there is a problem with the database
     * @return string
     */
    protected function createAlbumSlug($name) {
        return SlugUtility::generateSlug($name, function ($slug) {
            return $this->dbhandle->table('album')->where('slug', '=', $slug)->first();
        });
    }

    /**
     * Stores the album and its tracks in the database.
     * @param \KoflerDavid\TracklistsBundle\Entity\Album $album
     * @throws QueryException
     * @return bool
     */
    public function createAlbum(Album $album) {
        $this->dbhandle->getConnection()->beginTransaction();
        $slug = $this->createAlbumSlug($album->getName());

        $values = [ 'title' => $album->getName(), 'slug' => $slug ];
        try {
            $albumId = $this->dbhandle->table('album')->insertGetId($values);

            if (!is_numeric($albumId)) {
                $this->dbhandle->getConnection()->rollBack();

                return false;
            }
        } catch (QueryException $e) {
            $this->dbhandle->getConnection()->rollBack();

            // This error is thrown if the title is duplicated.
            if ((int)$e->getCode() === 23000) {
                return false;
            } else {
                throw $e;
            }
        }

        foreach ($album->getTracks() as &$track) {
            $record = [ 'title'         => $track->getName(), 'time_begin' => $track->getBegin()->format('H:i:s'),
                        'time_duration' => $track->getDuration()->format('%H:%I:%S'),
                        'label'         => $track->getLabel(), 'info' => $track->getInfo(),
                        'album_id'      => $albumId ];
            $id = $this->dbhandle->table('track')->insert($record);
            if (!$id) {
                $this->dbhandle->getConnection()->rollBack();

                return false;
            }

            $track->id = $id;
        }

        $this->dbhandle->getConnection()->commit();
        $album->setSlug($slug);
        $album->id = $albumId;
        $album->slug = $slug;

        return true;
    }

    /**
     * @param array $albumRecord
     * @param array $trackRecords
     * @return \KoflerDavid\TracklistsBundle\Entity\Album
     */
    public static function constructAlbumFromDatabase(array $albumRecord, array $trackRecords) {
        $tracks = [ ];
        foreach ($trackRecords as $trackRecord) {
            $tracks[] = TrackRepository::constructTrackFromDatabase($trackRecord, [ 'album_id', 'id' ]);
        }

        $album = new Album($albumRecord['title'], $albumRecord['slug'], $tracks, $albumRecord['path_location']);
        $album->id = $albumRecord['id'];
        $album->slug = $albumRecord['slug'];

        return $album;
    }

}
