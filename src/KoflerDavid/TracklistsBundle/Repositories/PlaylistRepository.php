<?php

namespace KoflerDavid\TracklistsBundle\Repositories;

use Illuminate\Database\QueryException;
use KoflerDavid\TracklistsBundle\Entity\Playlist;
use KoflerDavid\TracklistsBundle\Repository;
use KoflerDavid\TracklistsBundle\SlugUtility;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class PlaylistRepository extends Repository {

    protected $connection;

    /**
     * @return Playlist[]
     */
    public function all() {
        $playlistRecords = $this->dbhandle->table('playlist')->get();

        $playlists = [ ];
        foreach ($playlistRecords as $playlistRecord) {
            $playlists[] = self::constructFromDatabase($playlistRecord, [ ], [ ], [ ]);
        }

        return $playlists;
    }

    /**
     * Factory method to construct a playlist from records.
     * The additional parameters can be used to specify the mappings from
     * database fields to model attributes, in case a complicated query has to be used.
     *
     * @param array $playlistRecord
     * @param array $trackRecords
     * @param array $playlistAttributes mappings which specify which result set fields shall be stored on the playlist.
     * @param array $trackAttributes mappings which specify which result set fields shall be stored on the tracks.
     * @return Playlist
     */
    public static function constructFromDatabase(array $playlistRecord, array $trackRecords, array $playlistAttributes = [ ], array $trackAttributes = [ ]) {
        $tracks = [ ];
        foreach ($trackRecords as $trackRecord) {
            $tracks[] = TrackRepository::constructTrackFromDatabase($trackRecord, $trackAttributes);
        }

        $playlist = new Playlist($playlistRecord['title'], $tracks);
        $playlist->id = $playlistRecord['id'];
        $playlist->slug = $playlistRecord['slug'];
        $playlist->mapAndSetAttributes($playlistRecord, $playlistAttributes);

        return $playlist;
    }

    /**
     * @param int $id
     * @return false|Playlist
     */
    public function byId($id) {
        $playlistRecord = $this->dbhandle->table('playlist')->find($id);
        if ($playlistRecord !== null) {
            $trackRecords = $this->dbhandle->table('playlist_track')->join('track', 'playlist_track.track_id', '=', 'track.id')
                ->where('playlist_id', $id)->get();

            return self::constructFromDatabase($playlistRecord, $trackRecords, [ ], [ 'track.id'       => 'id',
                                                                                      'track.album_id' => 'album_id' ]);
        }

        return false;
    }

    /**
     * @param string $slug
     * @return false|Playlist
     */
    public function bySlug($slug) {
        $playlistRecord = $this->dbhandle->table('playlist')->where('slug', $slug)->first();
        if ($playlistRecord !== null) {
            $trackRecords = $this->dbhandle->table('playlist_track')->join('track', 'playlist_track.track_id', '=', 'track.id')
                ->where('playlist_id', $playlistRecord['id'])->get();

            return self::constructFromDatabase($playlistRecord, $trackRecords, [ ], [ 'track.id'       => 'id',
                                                                                      'track.album_id' => 'album_id' ]);
        }

        return false;
    }

    /**
     * @param int $trackId
     * @param int $playlistId
     * @return bool
     */
    public function addTrackToPlaylist($trackId, $playlistId) {
        return (bool)$this->dbhandle->table('tracklist_item')
            ->insert([ 'playlist_id' => $playlistId, 'track_id' => $trackId ]) > 0;
    }

    /**
     * @param $trackId
     * @param $playlistId
     * @return bool
     */
    public function removeTrackFromPlaylist($trackId, $playlistId) {
        return $this->dbhandle->table('playlist_item')->join('playlist', 'playlist_item.playlist_id', '=', 'playlist.id')
            ->where('playlist.id', '=', $playlistId)->where('track_id', $trackId)
            ->delete() > 0;
    }

    /**
     * Creates a new playlist. It is assumed that the tracks associated with the playlist already exist!
     *
     * @param Playlist $playlist
     * @return false|Playlist
     */
    public function store(Playlist $playlist) {
        $table = $this->dbhandle->table('playlist');
        $connection = $this->dbhandle->getConnection();

        try {
            $connection->beginTransaction();
            $slug = SlugUtility::generateSlug($playlist->getTitle(), function ($slug) use ($table) {
                return $table->where('slug', '=', $slug)->first() !== null;
            });

            $values = [ 'title' => $playlist->getTitle(), 'slug' => $slug ];

            $playlistId = $this->dbhandle->table('playlist')->insertGetId($values);
            $playlist->id = $playlistId;

            foreach ($playlist->getTracks() as $track) {
                $this->dbhandle->table('playlist_track')->insert([ 'playlist_id' => $playlistId,
                                                                   'track_id'    => $track->id ]);
            }

            $connection->commit();

            return $playlist;
            
        } catch (QueryException $e) {
            $connection->rollBack();

            // This error is thrown if a playlist with this title already exists.
            if ($e->getCode() == 23000) {
                return false;
            } else {
                throw $e;
            }
        }
    }

}
