<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/2/14
 * Time: 10:17 AM
 */

namespace KoflerDavid\TracklistsBundle\Repositories;

use KoflerDavid\TracklistsBundle\Repository;
use Tracklists\Repositories\User;

class UserRepository extends Repository {

    public function byId($id) {

    }

    public function byEmail($email) {

    }

    public function checkLogin($email, $password, $checkActive = true) {
        $fields = $this->dbhandle->table('user')->where('email', '=', $email)->first([ 'password_hash', 'active' ]);
        // The function must continue, else a timing attack to detect existing users becomes possible.
        if ($fields === null) {
            $fields = [ 'password_hash' => '', 'active' => 0 ];
        }

        $hash = $fields['password_hash'];
        $active = $fields['active'];

        // This order is important, else a timing attack to detect active users becomes possible!
        // Finally, the password_verify() function must be used to compare the hashes while avoiding a time-attack.
        return password_verify($password, $hash) && (!$checkActive || $active === 1);
    }

    public function constructUserFromDatabase($userRecord, $additionalAttributes) {
        $attributeMappings = [ 'id' => 'id' ];
        $attributeMappings = array_merge($attributeMappings, $additionalAttributes);
        $attributeMappings = array_unique($attributeMappings);

        $user = new User($userRecord['display_name'], $userRecord['email'], $userRecord['password_hash'], $userRecord['active'] === 1);

        return $user;
    }
} 