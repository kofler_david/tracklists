<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 10/13/14
 * Time: 4:02 PM
 */

namespace KoflerDavid\TracklistsBundle;


use Symfony\Component\HttpFoundation\ParameterBag;

class ValidationHelper {

    /**
     * Checks whether the specified parameter exists and represents a non-negative integer.
     * @param ParameterBag $parameters
     * @param string $name
     * @return bool
     */
    public static function isValidId(ParameterBag $parameters, $name) {
        $trackId = $parameters->get($name, false);

        // Usage of ctype_digit() checks whether there are only digits. If yes, the number is an integer.
        return $trackId !== false and strlen($trackId) > 0 and ctype_digit($trackId);
    }

    /**
     * Checks whether the identifier conforms to a quite restricted family of identifiers, valid for HTML attributes.
     * These consist of multiple alphanumeric ASCII-strings (no leading numbers and underscores) connected with dashes.
     *
     * @param string $identifier
     * @return bool
     */
    public static function isValidAsciiAttribute($identifier) {
        // Checks for multiple identifiers, connected with dashes
        return preg_match_all('/[a-zA-Z][a-zA-Z0-9_]*(-[a-zA-Z][a-zA-Z0-9_]*)*/', $identifier) === 1;
    }
}