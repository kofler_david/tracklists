<?php

namespace KoflerDavid\TracklistsBundle;

use League\Plates\Engine;

/**
 * Extends the \League\Plates\Extension\Asset class so that it prefixes
 * the base URLI to the assets on the webserver.
 *
 * @author David Kofler <kofler.david@gmail.com>
 */
class Asset extends \League\Plates\Extension\Asset {

    /**
     * @var Engine
     */
    protected $engine;

    /**
     * @var string
     */
    protected $assetUrl;

    /**
     * @param string $assetPath
     * @param string $assetUrl
     */
    public function __construct($assetPath, $assetUrl) {
        parent::__construct($assetPath);
        $this->assetUrl = rtrim($assetUrl, '/');
    }

    public function cachedAssetUrl($url) {
        // parent::cachedAssetUrl() always returns an absolute URI.
        return $this->assetUrl . parent::cachedAssetUrl($url);
    }

    public function assetDirectory($url) {
        return $this->assetUrl . '/' . ltrim($url, '/');
    }

    public function register(Engine $engine) {
        parent::register($engine);
        $engine->registerFunction('directory', [ $this, 'assetDirectory' ]);
    }

}
