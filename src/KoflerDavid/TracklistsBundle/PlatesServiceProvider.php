<?php

namespace KoflerDavid\TracklistsBundle;

use League\Plates\Engine;
use Silex\Application as SilexApplication;
use Silex\ServiceProviderInterface;

class PlatesServiceProvider implements ServiceProviderInterface {

    /**
     * @inheritdoc
     */
    public function register(SilexApplication $app) {
//        $app['plates.path'] = null;
//        $app['plates.file_extension'] = null;
//        $app['plates.default_data'] = [ ];


    }

    /**
     * @inheritdoc
     */
    public function boot(SilexApplication $app) {
        $app['plates'] = $app->share(function ($app) {
            $plates = new Engine($app['plates.path'], $app['plates.file_extension']);

            foreach ($app['plates.extensions'] as $extension) {
                $plates->loadExtension($extension);
            }

            if (!$app->offsetExists('plates.default_data')) {
                $app['plates.default_data'] = [ ];
            }

            $defaultData = array_replace([ 'app' => $app ], $app['plates.default_data']);
            $plates->addData($defaultData);

            return $plates;
        });
    }

}
