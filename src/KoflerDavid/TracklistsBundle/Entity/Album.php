<?php

namespace KoflerDavid\TracklistsBundle\Entity;

use KoflerDavid\TracklistsBundle\DateUtility;
use KoflerDavid\TracklistsBundle\Model;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class Album extends Model {

    /**
     * @var string
     */
    private $name;

    /**
     * @var Track[]
     */
    private $aoTrack;

    /**
     * @var string|null
     */
    private $pathLocation;

    /**
     * @param string $name
     * @param string $slug
     * @param Track[] $tracks
     * @param string|null $pathLocation
     */
    function __construct($name, $slug, array $tracks, $pathLocation) {
        $this->name = $name;
        $this->slug = $slug;
        $this->aoTrack = $tracks;
        $this->pathLocation = $pathLocation;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @return Track[]
     */
    public function getTracks() {
        return $this->aoTrack;
    }

    /**
     * @return string|null
     */
    public function getLocation() {
        return $this->pathLocation;
    }

    /**
     * @param string|null $slug
     * @return $this
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Calculates the beginning times for the album's tracks.
     * @return array the begin times. The first element will always be 0
     * because sometimes there is a short intro at the beginning.
     * Also if the album immediately starts with a track that element will be there.
     * (This is a more predictable behavior than omitting it according to the begin time of the first track)
     *
     * @return int[]
     */
    public function getBeginTimes() {
        $beginTimes = [ 0 ];
        foreach ($this->getTracks() as $track) {
            $beginTimes[] = DateUtility::timeToSeconds($track->getBegin());
        }

        return $beginTimes;
    }

    /**
     * @inheritdoc
     */
    function jsonSerialize() {
        $tracks = array_map(function (Track $track) {
            return $track->jsonSerialize();
        }, $this->aoTrack);

        return [ 'id'         => $this->id,
                 'title'      => $this->name,
                 'slug'       => $this->slug,
                 'location'   => $this->pathLocation,
                 'beginTimes' => $this->getBeginTimes(),
                 'tracks'     => $tracks ];
    }

}
