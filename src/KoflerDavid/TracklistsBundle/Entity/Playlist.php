<?php

namespace KoflerDavid\TracklistsBundle\Entity;

use KoflerDavid\TracklistsBundle\Model;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class Playlist extends Model {

    /**
     * @var string
     */
    protected $title;

    /**
     * @var Track[]
     */
    protected $aoTracks;

    /**
     * @param string $title
     * @param Track[] $aoTracks
     */
    public function __construct($title, array $aoTracks) {
        $this->title = $title;
        $this->aoTracks = $aoTracks;
    }

    /**
     * @return string
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * @return Track[]
     */
    public function getTracks() {
        return $this->aoTracks;
    }

    function jsonSerialize() {
        $tracks = array_map(function (Track $track) {
            return $track->jsonSerialize();
        }, $this->aoTracks);

        return [ 'id'     => $this->id,
                 'title'  => $this->getTitle(),
                 'slug'   => $this->slug,
                 'tracks' => $tracks
        ];
    }

}
