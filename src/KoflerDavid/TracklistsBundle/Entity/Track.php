<?php

namespace KoflerDavid\TracklistsBundle\Entity;

use Herrera\DateInterval\DateInterval;
use KoflerDavid\TracklistsBundle\Model;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class Track extends Model {

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $begin;

    /**
     * @var DateInterval
     */
    private $duration;

    /**
     * @var null|string
     */
    private $label;

    /**
     * @var null|string
     */
    private $info;

    /**
     * @var bool
     */
    private $flagLiked;

    /**
     * @param string $name
     * @param \DateTime $begin
     * @param DateInterval $duration
     * @param string|null $label
     * @param string|null $info
     * @param bool $flagLiked
     */
    function __construct($name, \DateTime $begin, DateInterval $duration, $label, $info, $flagLiked) {
        $this->name = $name;
        $this->begin = $begin;
        $this->duration = $duration;
        $this->label = $label;
        $this->info = $info;
        $this->flagLiked = $flagLiked;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return \DateTime
     */
    public function getBegin() {
        return $this->begin;
    }

    /**
     * @return DateInterval
     */
    public function getDuration() {
        return $this->duration;
    }

    /**
     * @return null|string
     */
    public function getLabel() {
        return $this->label;
    }

    /**
     * @return null|string
     */
    public function getInfo() {
        return $this->info;
    }

    /**
     * @return bool
     */
    public function isLiked() {
        return $this->flagLiked;
    }

    /**
     * @param DateInterval $duration
     * @return $this
     */
    public function setDuration($duration) {
        $this->duration = $duration;

        return $this;
    }

    /**
     * @inheritdoc
     */
    function jsonSerialize() {
        return [ 'id'        => $this->id,
                 'name'      => $this->name,
                 'begin'     => $this->begin->format('H:i:s'),
                 'duration'  => $this->duration->format('%H:%I:%S'),
                 'label'     => $this->label,
                 'info'      => $this->info,
                 'flagLiked' => (bool)$this->flagLiked ];
    }

}
