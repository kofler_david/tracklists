<?php

namespace KoflerDavid\TracklistsBundle\Entity;

use KoflerDavid\TracklistsBundle\Model;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class User extends Model {

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var bool
     */
    protected $active;

    /**
     * @param string $name
     * @param string $email
     * @param bool $active
     */
    function __construct($name, $email, $active) {
        $this->name = $name;
        $this->email = $email;
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * @return bool
     */
    public function isActive() {
        return $this->active;
    }

}