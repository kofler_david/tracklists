<?php

namespace KoflerDavid\TracklistsBundle;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class DateUtility {

    /**
     * @param string $timeString
     * @return false|\Herrera\DateInterval\DateInterval
     */
    public static function parseDateInterval($timeString) {
        $parts = [];
        $success = preg_match('/^(\d+):(\d+):(\d+)$/', $timeString, $parts);
        if ($parts) {
            return new \Herrera\DateInterval\DateInterval('PT'.$parts[1] . 'H' . $parts[2] . 'M' . $parts[3] . 'S');
        } else {
            return false;
        }
    }
    
    /**
     * Interpretes a \DateTime object as time and returns the time as seconds.
     * @param \DateTime $time
     * @return int the seconds since midnight of the date contained in $time.
     */
    public static function timeToSeconds(\DateTime $time) {
        return $time->format('H')*3600 + $time->format('i')*60 + $time->format('s');
    }
}
