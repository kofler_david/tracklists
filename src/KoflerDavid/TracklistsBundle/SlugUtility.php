<?php

namespace KoflerDavid\TracklistsBundle;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class SlugUtility {

    /**
     * Generates a slug out of the provided string. If the slug already exists then a new one will be generated.
     * @param string $name
     * @param callable $nameExists this callable should return true if the slug is already used.
     * @return string
     */
    public static function generateSlug($name, callable $nameExists) {
        $trimmedName = trim($name);
        $cleanedString = strtr($trimmedName, [ ' ' => '_', "\n" => '_', '.' => '_' ]);
        $slug = strtolower($cleanedString);

        $base = $slug;
        $i = 1;
        // If the slug already exists, append a number to generate a new one
        while (call_user_func($nameExists, $slug)) {
            // Increment the suffixed number
            $slug = $base . '_' . $i;
            ++$i;
        }

        return $slug;
    }

}
