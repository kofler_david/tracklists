<?php

namespace KoflerDavid\TracklistsBundle;

use Illuminate\Database\Capsule\Manager as Capsule;

class Repository {

    /**
     * @var \Illuminate\Database\Capsule\Manager
     */
    protected $dbhandle;

    /**
     * @param \Illuminate\Database\Capsule\Manager $dbhandle
     */
    public function __construct(Capsule $dbhandle) {
        $this->dbhandle = $dbhandle;
    }

} 