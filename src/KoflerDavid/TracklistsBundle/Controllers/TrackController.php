<?php

namespace KoflerDavid\TracklistsBundle\Controllers;

use KoflerDavid\TracklistsBundle\Controller;

/**
 * Description of TrackController
 *
 * @author David Kofler <kofler.david@gmail.com>
 */
class TrackController extends Controller {

    protected $repository;

    public function __construct(\Pimple $app) {
        parent::__construct($app);
        $this->repository = $app['repository.track'];
    }

    public function trackLike($id) {
        if ($id === null) {
            $id = $this->app['request']->query->get('id', null);
            if ($id === null) {
                return $this->app->json(['status' => 'error', 'message' => "The track to update was not specified."]);
            }
        }

        if ($this->repository->trackLike($id)) {
            return $this->app->json(['status' => 'success', 'message' => "Success"]);
        } else {
            return $this->app->json(['status' => 'error', 'message' => "Track doesn't exist"]);
        }
    }

    public function trackUnlike($id) {
        if ($id === null) {
            $id = $this->app['request']->query->get('id', null);
            if ($id === null) {
                return $this->app->json(['status' => 'error', 'message' => "The track to update was not specified."]);
            }
        }

        if ($this->repository->trackUnlike($id)) {
            return $this->app->json(['status' => 'success', 'message' => "Success"]);
        } else {
            return $this->app->json(['status' => 'failure', 'message' => "Track does not exist."]);
        }
    }
}
