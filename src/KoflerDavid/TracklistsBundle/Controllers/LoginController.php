<?php

namespace KoflerDavid\TracklistsBundle\Controllers;

/**
 * 
 *
 * @author David Kofler <kofler.david@gmail.com>
 */
class LoginController extends \KoflerDavid\TracklistsBundle\Controller {

    public function __construct(\Pimple $app) {
        parent::__construct($app);
        $this->exportAction(['doLogin', 'login'], ['get', 'post'], '/login');
    }

    public function doLogin() {
        //$destination, $queryArgs
        $destination = $this->app['request']->query->get('for');
        $this->render('login/login-form', 'Login', ['destination' => $destination, 'queryArgs' => $queryArgs]);
    }

    public function processLogin() {
        
    }

}
