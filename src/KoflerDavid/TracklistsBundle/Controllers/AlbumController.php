<?php

namespace KoflerDavid\TracklistsBundle\Controllers;

use Herrera\DateInterval\DateInterval;
use KoflerDavid\TracklistsBundle\Application;
use KoflerDavid\TracklistsBundle\Controller;
use KoflerDavid\TracklistsBundle\DateUtility;
use KoflerDavid\TracklistsBundle\Entity\Album;
use KoflerDavid\TracklistsBundle\Entity\Track;
use KoflerDavid\TracklistsBundle\Repositories\AlbumRepository;
use League\Csv\Writer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Tracklists;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class AlbumController extends Controller {

    const FILE_SELECTOR_PAGE_SIZE = 5;

    /**
     * @var AlbumRepository
     */
    protected $repository;

    /**
     * @param Application $app
     */
    public function __construct(Application $app) {
        parent::__construct($app);
        $this->repository = $app['repository.album'];
    }

    /**
     * Render the list of albums.
     * @return Response
     */
    public function albumList() {
        $albums = $this->repository->getAlbumList();

        return $this->render('album/list', 'Albums', [ 'albums' => $albums ]);
    }

    public function ajaxRenderAction(Request $request) {
        $albumId = $request->query->get('id', false);
        if ($albumId === false) {
            return $this->render('Album ID is missing', 'Illegal request', [ ], 400);
        }

        $album = $this->repository->byId($albumId);
        if ($album === false) {
            return $this->render('Album not found', 'Album not found', [ ], 404);
        }

        return $this->render('album/show.ajax', $album->getName(), [ 'album' => $album ]);
    }

    /**
     * @return Response
     */
    public function createAlbum() {
        $incomingFiles = $this->loadIncomingDirectory();

        return $this->render('album/create', 'Create Album', [ 'albumName' => '', 'tracks' => [ ], 'incomingFiles' => $incomingFiles ]);
    }

    /**
     * @return array[][][]
     */
    protected function loadIncomingDirectory() {
        $incoming = $this->app['incoming-directory'];
        // If the path of the directory for incoming files is not absolute, prepend the data path.
        if (strlen($incoming) > 0 and $incoming[0] !== '/') {
            $incoming = $this->app['data-path'] . '/' . $incoming;
        }

        $files = [ ];
        $iterator = new \FilesystemIterator($this->app['app_root'] . '/' . $incoming);
        foreach ($iterator as $name => $file) {
            if (!$file->isDir()) {
                $files[] = [ 'name' => $file->getBasename(), 'size' => $file->getSize() ];
            }
        }

        usort($files, function ($f1, $f2) {
            return strcmp($f1['name'], $f2['name']);
        });

        return array_chunk($files, self::FILE_SELECTOR_PAGE_SIZE);
    }

    /**
     *
     * @return Response
     */
    public function createAlbumPost() {
        $incomingFiles = $this->loadIncomingDirectory();

        return $this->render('album/create', 'Create Album', [ 'albumName' => '', 'tracks' => [ ], 'incomingFiles' => $incomingFiles ]);
    }

    /**
     * @return Response
     */
    public function parseAlbum() {
        $incomingFiles = $this->loadIncomingDirectory();

        return $this->render('album/parse', 'Parse album', [ 'albumName' => '', 'totalDuration' => '', 'input' => '', 'tracks' => [ ], 'incomingFiles' => $incomingFiles ]);
    }

    /**
     * This action either parses the raw album tracklist and fills it into a form so
     * that the user can verify the data, or it stores the album.
     * @return Response
     */
    public function parseAlbumPost() {
        $incomingFiles = $this->loadIncomingDirectory();

        $postParams = $this->app['request']->request;
        $name = $postParams->get('album-name');
        $totalDurationRaw = $postParams->get('totalDuration');
        $input = $postParams->get('input');

        // Detect which submit button was used
        if ($postParams->get('action') === 'save') {
            // Save the album
            $result = $this->saveAlbum($postParams, $name);

            if ($result['album'] !== false) {
                return $this->showAlbum($result['album']->slug);
            } else {
                $this->formErrors = array_replace($this->formErrors, $result['formErrors']);
                $this->errors = array_merge($this->errors, $result['errors']);

                $data = [ 'albumName'     => $name, 'totalDuration' => $totalDurationRaw,
                          'input'         => $input, 'tracks' => $result['tracks'],
                          'incomingFiles' => $incomingFiles ];

                return $this->render('album/parse', 'Parse album', $data);
            }

        } else {
            $totalDuration = \DateTime::createFromFormat('G:i:s', $totalDurationRaw);
            if ($totalDuration === false) {
                $this->formErrors['totalDuration'] = 'Invalid duration';

                $data = [ 'albumName'     => $name, 'totalDuration' => $totalDurationRaw,
                          'input'         => $input, 'tracks' => [ ],
                          'incomingFiles' => $incomingFiles ];

                return $this->render('album/parse', 'Parse album', $data);
            }

            //Parse the tracks.
            $tracks = static::parseTracklist($input, $totalDuration);

            $data = [ 'albumName'     => $name, 'totalDuration' => $totalDuration->format('H:i:s'),
                      'input'         => $input, 'tracks' => $tracks,
                      'incomingFiles' => $incomingFiles ];

            return $this->render('album/parse', 'Parse album', $data);
        }
    }

    /**
     * @param ParameterBag $postParams
     * @param string $name
     * @return array An array with the keys 'album', 'tracks', 'errors' and 'formErrors'
     */
    protected function saveAlbum($postParams, $name) {
        $result = static::parseTracksFromForm($postParams);

        $ret = [ 'album'      => false,
                 'tracks'     => $result['tracks'],
                 'errors'     => [ ],
                 'formErrors' => [ ] ];

        if (count($result['errors']) > 0) {
            // There is invalid data in the track form
            $ret['formErrors'] = $result['errors'];
        }

        if ($name == null || strlen(trim((string)$name)) == 0) {
            $ret['formErrors']['album-name'] = "Album title is missing";
        }

        if (count($ret['formErrors']) > 0) {
            return $ret;
        }

        $album = new Album($name, null, $result['tracks'], null);
        if ($this->repository->createAlbum($album)) {
            // Success
            $ret['album'] = $album;

            return $ret;

        } else {
            // Creating the album failed, and it's not because of a database malfunction.
            $ret['tracks'] = $album->getTracks(); // TODO: is this necessary to convince the form everything is valid?
            $ret['errors'] = [ "Could not create album because an album with the same title already exists" ];

            return $ret;
        }
    }

    /**
     * Reads in the tracks from the POST data.
     * @param ParameterBag $request the POST data sent by the client.
     * @return array[] an array, where the key 'tracks' refers to the tracks parsed and 'errors' contains the errors
     * encountered in the process.
     */
    protected static function parseTracksFromForm(ParameterBag $request) {
        $tracks = [ ];
        $errors = [ ];

        for ($i = 0; ($track = self::getTrack($request, $i)); ++$i) {
            $begin = \DateTime::createFromFormat('G:i:s', $track['begin']);
            if (!$begin) {
                $errors["track-$i-begin"] = "Invalid time string";
            }

            $duration = DateUtility::parseDateInterval($track['duration']);
            if (!$duration) {
                $errors[] = [ "track-$i-begin", "Invalid time string" ];
            }

            if ($begin and $duration) {
                $tracks[] = new Track($track['name'],
                    $begin,
                    $duration,
                    $track['label'],
                    $track['info'],
                    false);
            } else {
                $tracks[] = $track;
            }
        }

        return [ 'tracks' => $tracks, 'errors' => $errors ];
    }

    /**
     * @param ParameterBag $request
     * @param int $i
     * @return false|string[]
     */
    protected static function getTrack(ParameterBag $request, $i) {
        if ($request->has("track-$i-name")
            && $request->has("track-$i-begin")
            && $request->has("track-$i-duration")
            && $request->has("track-$i-label")
            && $request->has("track-$i-info")
        ) {
            return [ 'name'     => $request->get("track-$i-name"),
                     'begin'    => $request->get("track-$i-begin"),
                     'duration' => $request->get("track-$i-duration"),
                     'label'    => $request->get("track-$i-label"),
                     'info'     => $request->get("track-$i-info") ];
        }

        return false;
    }

    /**
     * Show the specified album to the user.
     * @param string $slug the slug of the album.
     * @return Response
     */
    public function showAlbum($slug) {
        $album = $this->repository->getAlbum($slug);
        if ($album) {
            if ($this->app['request']->query->get('format', false) === 'csv') {
                return $this->exportAlbumToCsv($album);
            }

            $path = $this->app['app_root'] . '/web' . $album->getLocation();
            $showPlayer = file_exists($path);

            return $this->render('album/show', $album->getName() ?: $slug, [ 'album' => $album, 'showPlayer' => $showPlayer, 'beginTimes' => $album->getBeginTimes() ]);
        } else {
            $albums = $this->repository->getAlbumList();

            return $this->render('album/list', 'Albums', [ 'albums' => $albums,
                                                           'errors' => [ '_' => 'Album not found' ] ]);
        }
    }

    /**
     * @param Album $album
     * @return Response
     */
    protected function exportAlbumToCsv(Album $album) {
        $csv = new Writer(new \SplTempFileObject());
        $csv->setEncoding('utf-8');
        $csv->setDelimiter(';');

        $i = 1;
        foreach ($album->getTracks() as $track) {
            $title = $track->getName();
            $begin = $track->getBegin()->format('H:i:s');
            $duration = $track->getDuration()->format('%H:%I:%S');
            $label = $track->getLabel();
            $info = $track->getInfo();
            $liked = $track->isLiked() ? '*' : '';

            $csv->insertOne([ $i, $title, $begin, $duration, $info, $label, $liked ]);
            ++$i;
        }

        $filename = $album->slug . '.csv';

        return new Response($csv, 200, [ 'Content-Type'        => 'text/css; charset=utf-8',
                                         'Content-Disposition' => 'attachment; filename=' . $filename ]);
    }

    protected static function parseTracklist($input, \DateTime $totalDuration) {
        $lines = explode("\n", $input);

        $beginnings = [ ];
        $tracks = [ ];
        foreach ($lines as $line) {
            $track = static::parseTracklistLine($line);
            if ($track) {
                $tracks[] = $track;
                $beginnings[] = $track->getBegin();
            }
        }

        $beginnings[] = $totalDuration;
        // The beginning of the second track is assumed to be the end of the first track.
        next($beginnings);
        foreach ($tracks as &$track) {
            // Initially the only information available is the beginning of each title.
            // Out of that each title's duration mast be calculated.
            $track->setDuration(current($beginnings)->diff($track->getBegin()));
            next($beginnings);
        }

        return $tracks;
    }

    /**
     * Helper function which parses a tracklist as seen on
     * https://soundcloud.com/oriuplift/uponly-073 into tracks.
     * @param string $line
     * @return false|Track
     */
    protected static function parseTracklistLine($line) {
        $line = trim($line);
        $matches = [ ];

        $regex1 = '/^\d+\. \[(?P<begin>\d+:\d+:\d+)\]: (?P<info>.*): (?P<name>[^[]*) \[(?P<label>[^]]*)\]/';
        $regex2 = '/^\d+\. \[(?P<begin>\d+:\d+:\d+)\]: (?P<name>[^[]*) \[(?P<label>[^]]*)\]/';

        if (preg_match($regex1, $line, $matches) === 1) {
            return new Track($matches['name'], \DateTime::createFromFormat('H:i:s', $matches['begin']), DateInterval::fromSeconds(0), $matches['label'], $matches['info'], false);
        } else if (preg_match($regex2, $line, $matches) === 1) {
            return new Track($matches['name'], \DateTime::createFromFormat('H:i:s', $matches['begin']), DateInterval::fromSeconds(0), $matches['label'], '', false);
        }

        return false;
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function ajaxGetAlbum($id) {
        $album = $this->repository->byId($id);
        if (!$album) {
            return $this->ajaxResponse(null, "The album with the specified ID doesn't exist", 404);
        }

        $data = [
            'id'         => $album->id,
            'title'      => $album->getName(),
            'beginTimes' => $album->getBeginTimes(),
            'location'   => $album->getLocation()
        ];

        return $this->ajaxResponse($data);
    }
}
