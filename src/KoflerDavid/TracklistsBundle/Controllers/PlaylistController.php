<?php

namespace KoflerDavid\TracklistsBundle\Controllers;

use KoflerDavid\TracklistsBundle\Application;
use KoflerDavid\TracklistsBundle\Controller;
use KoflerDavid\TracklistsBundle\Entity\Playlist;
use KoflerDavid\TracklistsBundle\Repositories\PlaylistRepository;
use KoflerDavid\TracklistsBundle\ValidationHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author David Kofler <kofler.david@gmail.com>
 */
class PlaylistController extends Controller {

    /**
     * @var PlaylistRepository
     */
    protected $repository;

    public function __construct(Application $app) {
        parent::__construct($app);

        $this->repository = $app['repository.playlist'];
    }

    /**
     * Lists  all playlists and allows to display, to play and to delete them.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction() {
        $playlists = $this->repository->all();

        return $this->render('playlist/index', 'All Playlists', [ 'playlists' => $playlists ]);
    }

    /**
     * Called by the player to display available playlists.
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxIndexAction() {
        $playlists = $this->repository->all();

        return $this->ajaxResponse($playlists, "All playlists");
    }

    /**
     * Displays a playlist and its tracks and allows to play, to change and to delete it.
     *
     * @param string $slug
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($slug) {
        $playlist = $this->repository->bySlug($slug);
        if (!$playlist) {
            $this->errors[] = "The specified playlist does not exists";

            return $this->playlistIndexAction();
        }

        return $this->render('playlist/show', $playlist->getTitle(), [ 'playlist' => $playlist ]);
    }

    /**
     * Called by the player to fetch a playlist.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxFetchAction(Request $request) {
        if (!ValidationHelper::isValidId($request->query, 'playlistId')) {
            return $this->ajaxResponse(null, "Missing or invalid playlist ID", 400);
        }

        $playlistId = $request->query->getInt('playlistId');
        $playlist = $this->repository->byId($playlistId);
        if (!$playlist) {
            return $this->ajaxResponse([ 'playlistId' => $playlistId ], "Playlist $playlistId not found", 404);
        }

        return $this->ajaxResponse($playlist, "Found playlist $playlistId");
    }

    /**
     * Creates a modal which is used to ask the name of a new playlist from the user.
     *
     * @param Request $request
     * @return Response
     */
    public function ajaxNewAction(Request $request) {
        $idPrefix = $request->query->get('idPrefix', '');
        if (!ValidationHelper::isValidAsciiAttribute($idPrefix)) {
            return new Response('Invalid idPrefix: ' . json_encode($idPrefix), 400);
        }

        $destination = $request->query->get('destination', $this->app->url('home'));

        return $this->render('playlist/new.ajax', '', [ 'destination' => $destination,
                                                        'idPrefix'    => $idPrefix ]);
    }

    /**
     * Called to create a playlist.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxCreateAction(Request $request) {
        $desiredTitle = $request->request->get('title');
        if ($desiredTitle === null || strlen($desiredTitle) == 0) {
            return $this->ajaxResponse(null, 'No playlist name specified', 400);
        }

        $playlist = new Playlist($desiredTitle, [ ]);
        if ($this->repository->store($playlist)) {
            return $this->ajaxResponse($playlist, 'Playlist was created');
        } else {
            return $this->ajaxResponse([ 'title' => $desiredTitle ], 'Duplicate playlist title', 400);
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxAddTrackAction(Request $request) {
        if (!ValidationHelper::isValidId($request->query, 'trackId')) {
            return $this->ajaxResponse(null, "Missing or invalid track ID", 400);
        }

        if (!ValidationHelper::isValidId($request->query, 'playlistId')) {
            return $this->ajaxResponse(null, "Missing or invalid playlist ID", 400);
        }

        $trackId = $request->query->getInt('trackId');
        $playlistId = $request->query->getInt('playlistId');

        if ($this->repository->addTrackToPlaylist($trackId, $playlistId)) {
            return $this->ajaxResponse([ 'trackId' => $trackId, 'playlistId' => $playlistId ],
                "Track $trackId was added to playlist $playlistId");
        } else {
            return $this->ajaxResponse([ 'trackId' => $trackId, 'playlistId' => $playlistId ],
                "The track $trackId could not be added to playlist $playlistId", 400);
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function ajaxRemoveTrackAction(Request $request) {
        // Usage of ctype_digit() checks whether there are only digits. If yes, the number is an integer.
        if (!ValidationHelper::isValidId($request->query, 'trackId')) {
            return $this->ajaxResponse(null, "Missing or invalid track ID", 400);
        }

        $playlistId = (int)$this['request']->query->get('playlistId');
        if (!ValidationHelper::isValidId($request->query, 'playlistId')) {
            return $this->ajaxResponse(null, "Missing or invalid playlist ID", 400);
        }

        $trackId = $request->query->getInt('trackId');
        $playlistId = $request->query->getInt('playlistId');
        if ($this->repository->removeTrackFromPlaylist($playlistId, $trackId)) {
            return $this->ajaxResponse([ 'trackId' => $trackId, 'playlistId' => $playlistId ],
                "Track $trackId was removed from playlist $playlistId");
        } else {
            return $this->ajaxResponse([ 'trackId' => $trackId, 'playlistId' => $playlistId ],
                "The track $trackId could not be removed from playlist $playlistId", 400);
        }
    }

}
