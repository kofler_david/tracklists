CREATE TABLE user (
  id            INTEGER AUTO_INCREMENT,
  display_name  VARCHAR(100) NOT NULL,
  email         VARCHAR(100) NOT NULL,
  password_hash VARCHAR(200) NOT NULL,
  active        BOOLEAN      NOT NULL DEFAULT FALSE,

  PRIMARY KEY (id),
  UNIQUE (display_name),
  UNIQUE (email)
)
  ENGINE = InnoDb
  DEFAULT CHARSET =utf8;

-- INSERT INTO user (display_name, email, password_hash, active) VALUES ('System', 'this.login@wont.work.com', '', TRUE);
-- INSERT INTO user (display_name, email, password_hash, active) VALUES ('Root', 'please@dont.login.com', 'willbechanged', FALSE);

CREATE TABLE album (
  id            INTEGER AUTO_INCREMENT,
  title         VARCHAR(150) NOT NULL,
  slug          VARCHAR(100) NOT NULL,
  path_location VARCHAR(255),

  PRIMARY KEY (id),
  UNIQUE (title),
  UNIQUE (slug)
)
  ENGINE = InnoDb
  DEFAULT CHARSET =utf8;

CREATE TABLE track (
  id            SERIAL AUTO_INCREMENT,
  album_id      INTEGER      NOT NULL,
  title         VARCHAR(150) NOT NULL,
  time_begin    TIME,
  time_duration TIME,
  info          VARCHAR(200),
  label         VARCHAR(100),
  flag_liked    BOOLEAN      NOT NULL DEFAULT '0',

  PRIMARY KEY (id),
  FOREIGN KEY (album_id) REFERENCES album (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)
  ENGINE = InnoDb
  DEFAULT CHARSET =utf8;

CREATE TABLE playlist (
  id    INTEGER AUTO_INCREMENT,
  title VARCHAR(150) NOT NULL,
  slug  VARCHAR(150) NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (title),
  UNIQUE (slug)
)
  ENGINE = InnoDb
  DEFAULT CHARSET =utf8;

CREATE TABLE playlist_track (
  id          INTEGER AUTO_INCREMENT,
  playlist_id INTEGER         NOT NULL,
  track_id    BIGINT UNSIGNED NOT NULL,

  PRIMARY KEY (id),
  KEY (playlist_id),
  FOREIGN KEY (playlist_id) REFERENCES playlist (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE,
  FOREIGN KEY (track_id) REFERENCES track (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)
  ENGINE = InnoDb
  DEFAULT CHARSET =utf8;
