<?php

namespace KoflerDavid\TracklistsBundle;


use Symfony\Component\HttpFoundation\ParameterBag;

class ValidationHelperTest extends \PHPUnit_Framework_TestCase {

    const EMPTY_STRING = '';
    const IDENTIFIER = 'identifier';

    public function testEmptyStringIsInvalidId() {
        $bag = new ParameterBag([ self::IDENTIFIER => self::EMPTY_STRING ]);
        self::assertFalse(ValidationHelper::isValidId($bag, self::IDENTIFIER));
    }

    public function testRealNumberIsInvalidId() {
        $bag = new ParameterBag([ self::IDENTIFIER => '2.0' ]);
        self::assertFalse(ValidationHelper::isValidId($bag, self::IDENTIFIER));
    }

    public function testIntegerIsValidId() {
        $bag = new ParameterBag([ self::IDENTIFIER => '2' ]);
        self::assertTrue(ValidationHelper::isValidId($bag, self::IDENTIFIER));
    }

    public function testEmptyStringIsInvalidHtmlAttribute() {
        self::assertFalse(ValidationHelper::isValidAsciiAttribute(self::EMPTY_STRING));
    }

    public function testAlphabeticStringsIsValidHtmlAttribute() {
        self::assertTrue(ValidationHelper::isValidAsciiAttribute('modalId'));
    }

    public function testDashedStringIsValidHtmlAttribute() {
        self::assertTrue(ValidationHelper::isValidAsciiAttribute('modal-id'));
    }
}