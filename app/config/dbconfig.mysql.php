<?php

return ['driver' => 'mysql',
		'host' => 'localhost',
		'database' => 'tracklists',
		'username' => 'tracklists',
		'password' => 'tracklistspassword',
		'charset' => 'utf8',
		'collation' => 'utf8_unicode_ci'];
