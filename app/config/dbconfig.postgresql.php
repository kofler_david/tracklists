<?php

return ['driver' => 'postgresql',
		'host' => 'localhost',
		'database' => 'tracklists',
		'user' => 'tracklists',
		'password' => 'tracklistspassword'];