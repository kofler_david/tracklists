<?php

require_once __DIR__ . '/bootstrap.php';

$app = new \KoflerDavid\TracklistsBundle\Application([
    'app_root'    => dirname(__DIR__),
    'root-url'    => dirname($_SERVER['SCRIPT_NAME']),
    'config-file' => __DIR__ . '/config/config.json' ]);

$app['env'] = array_key_exists('env', $_ENV) ? $_ENV['env'] : 'dev';
if ('production' !== $app['env']) {
    $app['debug'] = true;
}

return $app;